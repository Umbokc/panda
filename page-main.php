<?php $u_assets_path_main = get_template_directory_uri() . '/assets/main'; ?>
<?php $u_site_url = get_site_url()  ?>

<!doctype html>
<html class="no-js" lang="rus">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Panda Media</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="<?= $u_assets_path_main ?>/img/favicon.ico">
	
	<!-- styles file -->
	<link rel="stylesheet" href="<?= $u_assets_path_main ?>/style/components/css/vendor.css">

</head>

<body>
	<!-- PRELOADER START -->
	<div class="preloader">
		<div class="loader">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
	<!-- PRELOADER START -->
	<!-- PHOTOSWIPE CODE START -->
	<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="pswp__bg"></div>
		<div class="pswp__scroll-wrap">
			<div class="pswp__container">
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
			</div>
			<div class="pswp__ui pswp__ui--hidden">
				<div class="pswp__top-bar">
					<div class="pswp__counter"></div>
					<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
					<button class="pswp__button pswp__button--share" title="Share"></button>
					<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
					<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
					<div class="pswp__preloader">
						<div class="pswp__preloader__icn">
							<div class="pswp__preloader__cut">
								<div class="pswp__preloader__donut"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
					<div class="pswp__share-tooltip"></div> 
				</div>
				<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
				</button>
				<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
				</button>
				<div class="pswp__caption">
					<div class="pswp__caption__center"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- PHOTOSWIPE CODE END -->
	<!-- WRAPPER SECTION START -->
	<section id="wrapper" class="wrapper">
<!-- 		<div class="left-sidebar">
			<div class="logo">
				<h1 class="u-ff@jso">
					<span>media</span>
					<span class="panda">panda</span>
				</h1>
			</div>
		</div> -->
		<!-- PARTICLE BACKGROUND START -->
		<div id="particles-js"></div> 
		<!-- PARTICLE BACKGROUND END -->

		<!-- WRAPPER LEFT SECTION START -->
		<a class="u-posa-lh-b0" href="tg://resolve?domain=<ko_ko_konst>"><i style="color:#fff; font-size: 20px;" class="ion-paper-airplane"></i></a>
		<div class="left-side">
		</div>
		<!-- WRAPPER LEFT SECTION END -->

		<!-- WRAPPER RIGHT SECTION START -->
		<div class="right-side">
			
			<!-- navigation start -->
			<nav>
				<button class="menu-btn">
					<svg version="1.1" id="menuicn" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 54 33" enable-background="new 0 0 54.238 33" xml:space="preserve">
						<rect y="30" class="first" width="54.238" height="3" />
						<rect x="0" y="20" class="inbetween" width="44.238" height="3" />
						<rect x="0" y="10" class="inbetween" width="34.238" height="3" />
						<rect width="54.238" class="last" height="3" />
					</svg>
				</button>
				<ul>
					<li data-menuanchor="home"><a href="#home">главная</a></li>
					<li data-menuanchor="about"><a href="#about">о компании</a></li>
					<li data-menuanchor="skills"><a href="#skills">навыки</a></li>
					<li data-menuanchor="services"><a href="#services">услуги</a>
						<ul class="sub-nav">
							<li><a href="<?= $u_site_url ?>/services-seo/" target="_blank">СЕО</a></li>
							<li><a href="<?= $u_site_url ?>/services/?id=1" target="_blank">Ваш рекламщик</a></li>
							<li><a href="<?= $u_site_url ?>/services/?id=2" target="_blank">Контекст</a></li>
							<li><a href="<?= $u_site_url ?>/services-smm/" target="_blank">SMM</a></li>
							<li><a href="<?= $u_site_url ?>/services/?id=4" target="_blank">Наружка</a></li>
							<li><a href="<?= $u_site_url ?>/services/?id=5" target="_blank">Digital</a></li>
							<li><a href="<?= $u_site_url ?>/services-site/" target="_blank">Сайты</a></li>
						</ul>
					</li>
					<li data-menuanchor="portfolio"><a href="#portfolio">портфолио</a></li>
					<li data-menuanchor="news"><a href="#news">новости</a></li>
					<li data-menuanchor="contacts"><a href="#contacts">контакты</a></li>
				</ul>
			</nav>
			<!-- navigation end -->

			<!-- FULLPAGE WRAPPER START -->
			<div id="fullpage">

				<!-- HOME SECTION START -->
				<div class="section home">
					<!-- text section start -->
					<div class="text-section">
						<div class="pre-title">
							Мы креативное агенство
						</div>
						<!-- text rotator start -->
						<div class="name-title">
							 Smm, SEO, Direct, Video, WOW-Design!
						</div>
						<!-- text rotator end -->
						<p class="descrip">
							Воплощаем мечты в digital реальность.<br /> Быстро. Креативно, а главное – с любовью.
						</p>
					</div>
					<!-- text section end -->

					<!-- social networks section start -->
					<ul class="social-section">
						<li><a href="tg://resolve?domain=<ko_ko_konst>"><i class="ion-paper-airplane"></i></a></li>
						<li><a href="info@mediapanda.ru"><i class="ion-email"></i></a></li>
					</ul>
					<!-- social networks section end -->
				</div>
				<!-- HOME SECTION END -->

				<!-- ABOUT SECTION START -->
				<div class="section about">

					<!-- text section start -->
					<div class="text-section">
						<div class="bg-title">О нас</div>
						<div class="title">
							О компании
						</div>
						<p class="descrip">
							Диджитал-агентство Media Panda занимает лидирующие позиции в своей отрасли и зарекомендовала себя как надежный партнер, при этом Компания продолжает успешное развитие во всех сегментах и видит широкие возможности для дальнейшей экспансии на другие рынки.
						</p>
					</div>
					<!-- text section end -->

					<!-- contact information start -->
					<ul class="contact-inf">
						<li><i class="ion-ios-location"></i> Россия, г. Москва, ул. Щербаковская 3. оф.704</li>
						<li><i class="ion-ios-telephone"></i> +7(926)649-49-84</li>
						<li><i class="ion-email"></i> info@mediapanda.ru</li>
					</ul>
					<!-- contact information end -->

				</div>
				<!-- ABOUT SECTION END -->

				<!-- SKILLS SECTION START -->
				<div class="section skills">

					<!-- text section start -->
					<div class="text-section">
						<div class="bg-title">Умения</div>
						<div class="title">
							Наши навыки
						</div>
					</div>
					<!-- text section end -->

					<!-- SKILL BLOCK WRAPPER START-->
					<div class="skill-block">

						<!-- skill item start-->
						<div class="skill-item" data-skill="95%">
							<h1>Интернет-маркетинг</h1>
							<div class="skill-bar"></div>
							<div class="skill-percent">95%</div>
						</div>
						<!-- skill item end-->

						<!-- skill item start-->
						<div class="skill-item" data-skill="93%">
							<h1>SEO</h1>
							<div class="skill-bar"></div>
							<div class="skill-percent">93%</div>
						</div>
						<!-- skill item end-->

						<!-- skill item start-->
						<div class="skill-item" data-skill="90%">
							<h1>Контекст</h1>
							<div class="skill-bar"></div>
							<div class="skill-percent">90%</div>
						</div>
						<!-- skill item end-->

						<!-- skill item start-->
						<div class="skill-item" data-skill="97%">
							<h1>СММ</h1>
							<div class="skill-bar"></div>
							<div class="skill-percent">97%</div>
						</div>
						<!-- skill item end-->
					</div>
					<!-- SKILL BLOCK WRAPPER END-->

					<!-- COUNTERS BLOCK WRAPPER START-->
					<div class="counters-block">

						<!-- counter item start-->
						<div class="counter-item">
							<i class="ion-coffee"></i>
							<span data-count="645">0</span>
							<h2>кружек кофе выпито</h2>
						</div>
						<!-- counter item end-->

						<!-- counter item start-->
						<div class="counter-item">
							<i class="ion-images"></i>
							<span data-count="148">0</span>
							<h2>рекламных кампаний создано</h2>
						</div>
						<!-- counter item end-->

						<!-- counter item start-->
						<div class="counter-item">
							<i class="ion-code-working"></i>
							<span data-count="32494">0</span>
							<h2>кодов аналитики написано</h2>
						</div>
						<!-- counter item end-->

						<!-- counter item start-->
						<div class="counter-item">
							<i class="ion-compose"></i>
							<span data-count="645">0</span>
							<h2>проектов запущено</h2>
						</div>
						<!-- counter item end-->
					</div>
					<!-- COUNTERS BLOCK WRAPPER END-->

				</div>
				<!-- SKILLS SECTION END -->

				<!-- SERVICES SECTION START -->
				<div class="section services">

					<!-- text section start -->
					<div class="text-section">
						<div class="bg-title">сервис</div>
						<div class="title">
							Наши услуги
						</div>
					</div>
					<!-- text section end -->

					<!-- SERVICES CAROUSEL WRAPPER START -->
					<div class="slick-carousel">

						<!-- services item start -->
						<a href="<?= $u_site_url ?>/services-seo/" target="_blank">
							<div class="item" data-tilt>
									<div class="hexagon">
										<div class="left"></div>
										<div class="center"></div>
										<div class="right"></div>
									</div>
									<div class="content">
										<h1>СЕО</h1>
										<p>Интересует полезные трафик на сайт мы приносим доход и непустые посещения</p>
									</div>
							</div>
						</a>
						<!-- services item end -->

						<!-- services item start -->
						<a href="<?= $u_site_url ?>/services/?id=1" target="_blank">
							<div class="item" data-tilt>
								<div class="hexagon">
									<div class="left"></div>
									<div class="center"></div>
									<div class="right"></div>
								</div>
								<div class="content">
									<h1>Ваш рекламщик</h1>
									<p>Продумываем имидж компании от А до Я, занимаемся репутацией вашей организации на просторах интернета.</p>
								</div>
							</div>
						</a>
						<!-- services item end -->

						<!-- services item start -->
						<a href="<?= $u_site_url ?>/services/?id=2" target="_blank">
							<div class="item" data-tilt>
								<div class="hexagon">
									<div class="left"></div>
									<div class="center"></div>
									<div class="right"></div>
								</div>
								<div class="content">
									<h1>Контекст</h1>
									<p>Нас интересует ваш заработок. Только оплата за клиента а не за клики!</p>
								</div>
							</div>
						</a>
						<!-- services item end -->

						<!-- services item start -->
						<a href="<?= $u_site_url ?>/services-smm/" target="_blank">
							<div class="item" data-tilt>
								<div class="hexagon">
									<div class="left"></div>
									<div class="center"></div>
									<div class="right"></div>
								</div>
								<div class="content">
									<h1>SMM</h1>
									<p>Не верите в социальной сети? Тогда мы идем к вам! Минимум вложений максимум прибыли.</p>
								</div>
							</div>
						</a>
						<!-- services item end -->

						<!-- services item start -->
						<a href="<?= $u_site_url ?>/services/?id=4" target="_blank">
							<div class="item" data-tilt>
								<div class="hexagon">
									<div class="left"></div>
									<div class="center"></div>
									<div class="right"></div>
								</div>
								<div class="content">
									<h1>Наружка</h1>
									<p>Билборды, ТВ, LED экраны, реклама на транспорте. Верстка дизайна бесплатно!</p>
								</div>
							</div>
						</a>
						<!-- services item end -->

						<!-- services item start -->
						<a href="<?= $u_site_url ?>/services/?id=5" target="_blank">
							<div class="item" data-tilt>
								<div class="hexagon">
									<div class="left"></div>
									<div class="center"></div>
									<div class="right"></div>
								</div>
								<div class="content">
									<h1>Digital</h1>
									<p>Пишем сценарии, снимаем, монтируем, запускаем массы!</p>
								</div>
							</div>
						</a>
						<!-- services item end -->

						<!-- services item start -->
						<a href="<?= $u_site_url ?>/services-site/" target="_blank">
							<div class="item" data-tilt>
								<div class="hexagon">
									<div class="left"></div>
									<div class="center"></div>
									<div class="right"></div>
								</div>
								<div class="content">
									<h1>Разработка сайтов</h1>
									<p>Красивые конверсионные? теперь это стало реальностью!</p>
								</div>
							</div>
						</a>
						<!-- services item end -->

					</div>
					<!-- SERVICES CAROUSEL WRAPPER END -->

				</div>
				<!-- SERVICES SECTION END -->

				<!-- PORTFOLIO SECTION START -->
				<div class="section portfolio">

					<!-- text section start -->
					<div class="text-section">
						<div class="bg-title">Кейсы</div>
						<div class="title">
							ПОРТФОЛИО
						</div>
					</div>
					<!-- text section end -->

					<!-- PORTFOLIO CAROUSEL WRAPPER START -->
					<div class="owl-carousel u-pswp-elements">

						<!-- portfolio item start -->
						<div class="item">
							<img src="<?= $u_assets_path_main ?>/img/portfolio/1.jpg" alt="">
							<div class="ovrlyT"></div>
							<div class="ovrlyB"></div>
							<span class="proj-name">Название проекта</span>
							<a class="proj-btn">
								<i class="ion-ios-browsers-outline"></i>
							</a>
						</div>
						<!-- portfolio item end -->

						<!-- portfolio item start -->
						<div class="item">
							<img src="<?= $u_assets_path_main ?>/img/portfolio/2.jpg" alt="">
							<div class="ovrlyT"></div>
							<div class="ovrlyB"></div>
							<span class="proj-name">Название проекта</span>
							<a class="proj-btn">
								<i class="ion-ios-browsers-outline"></i>
							</a>
						</div>
						<!-- portfolio item end -->

						<!-- portfolio item start -->
						<div class="item">
							<img src="<?= $u_assets_path_main ?>/img/portfolio/3.jpg" alt="">
							<div class="ovrlyT"></div>
							<div class="ovrlyB"></div>
							<span class="proj-name">Название проекта</span>
							<a class="proj-btn">
								<i class="ion-ios-browsers-outline"></i>
							</a>
						</div>
						<!-- portfolio item end -->

						<!-- services item start -->
						<div class="item">
							<img src="<?= $u_assets_path_main ?>/img/portfolio/4.jpg" alt="">
							<div class="ovrlyT"></div>
							<div class="ovrlyB"></div>
							<span class="proj-name">Название проекта</span>
							<a class="proj-btn">
								<i class="ion-ios-browsers-outline"></i>
							</a>
						</div>
						<!-- portfolio item end -->

						<!-- portfolio item start -->
						<div class="item">
							<img src="<?= $u_assets_path_main ?>/img/portfolio/5.jpg" alt="">
							<div class="ovrlyT"></div>
							<div class="ovrlyB"></div>
							<span class="proj-name">Название проекта</span>
							<a class="proj-btn">
								<i class="ion-ios-browsers-outline"></i>
							</a>
						</div>
						<!-- portfolio item end -->

						<!-- portfolio item start -->
						<div class="item">
							<img src="<?= $u_assets_path_main ?>/img/portfolio/1.jpg" alt="">
							<div class="ovrlyT"></div>
							<div class="ovrlyB"></div>
							<span class="proj-name">Название проекта</span>
							<a class="proj-btn">
								<i class="ion-ios-browsers-outline"></i>
							</a>
						</div>
						<!-- portfolio item end -->

						<!-- portfolio item start -->
						<div class="item">
							<img src="<?= $u_assets_path_main ?>/img/portfolio/4.jpg" alt="">
							<div class="ovrlyT"></div>
							<div class="ovrlyB"></div>
							<span class="proj-name">Название проекта</span>
							<a class="proj-btn">
								<i class="ion-ios-browsers-outline"></i>
							</a>
						</div>
						<!-- portfolio item end -->

						<!-- portfolio item start -->
						<div class="item">
							<img src="<?= $u_assets_path_main ?>/img/portfolio/3.jpg" alt="">
							<div class="ovrlyT"></div>
							<div class="ovrlyB"></div>
							<span class="proj-name">Название проекта</span>
							<a class="proj-btn">
								<i class="ion-ios-browsers-outline"></i>
							</a>
						</div>
						<!-- portfolio item end -->

						<!-- portfolio item start -->
						<div class="item">
							<img src="<?= $u_assets_path_main ?>/img/portfolio/2.jpg" alt="">
							<div class="ovrlyT"></div>
							<div class="ovrlyB"></div>
							<span class="proj-name">Название проекта</span>
							<a class="proj-btn">
								<i class="ion-ios-browsers-outline"></i>
							</a>
						</div>
						<!-- portfolio item end -->

						<!-- portfolio item start -->
						<div class="item">
							<img src="<?= $u_assets_path_main ?>/img/portfolio/1.jpg" alt="">
							<div class="ovrlyT"></div>
							<div class="ovrlyB"></div>
							<span class="proj-name">Название проекта</span>
							<a class="proj-btn">
								<i class="ion-ios-browsers-outline"></i>
							</a>
						</div>
						<!-- portfolio item end -->
					</div>
					<!-- PORTFOLIO CAROUSEL WRAPPER END -->

				</div>
				<!-- PORTFOLIO SECTION END -->

				<!-- NEWS SECTION START -->
				<div class="section news">

					<!-- text section start -->
					<div class="text-section" style="margin-top: 1vh;">
						<div class="bg-title">Блог</div>
						<div class="title">
							Новости
						</div>
						<p class="descrip">
							Диджитал-агентство Media Panda занимает лидирующие позиции в своей отрасли и зарекомендовала себя как надежный партнер, при этом Компания продолжает успешное развитие во всех сегментах и видит широкие возможности для дальнейшей экспансии на другие рынки.
						</p>
					</div>
					<!-- text section end -->
					<h3>
						Свежие публикации
					</h3>
					<!-- features posts start -->
					<div class="features-posts">
						<?php
							global $post;
							$args = array( 'posts_per_page' => 5, 'offset'=> 1);
							$myposts = get_posts( $args );
							$i = 0; 
							foreach( $myposts as $post ){ setup_postdata($post); if ( !has_post_thumbnail() ) continue;
								if($i >= 2) break; ++$i;
								?>
								<div class="item">
									<a href="<?= get_permalink(); ?>" target="_blank">
										<img width=100 height=75 style="object-fit: cover;" src="<?php the_post_thumbnail_url(array(50, 50));?>" alt="">
									</a>
									<a href="<?= get_permalink(); ?>" target="_blank"><p class="title"><?php the_title(); ?></p></a>
									<p class="date"><?php the_time('M d, Y'); ?></p>
								</div>
								<?php
							}
							wp_reset_postdata();
						?>
					</div>
					<!-- features posts end -->
					
					<a href="./blog" class="btn features-posts-btn u:tdn" target="_blank">
						Перейти
					</a>
				</div>
				<!-- NEWS SECTION END -->

				<!-- CONTACTS SECTION START -->
				<div class="section contacts">

					<!-- text section start -->
					<div class="text-section">
						<div class="bg-title">Контакты</div>
						<div class="title">
							Обращайтесь
						</div>
					</div>
					<!-- text section end -->

					<!-- CONTACT FORM WRAPPER START -->
					<div class="contact-form">
						<form id="contactForm" data-toggle="validator" data-post="<?= get_template_directory_uri() ?>/mail.php">
							<div class="row">
								<div class="field form-group col-sm-6">
									<label for="name">Имя</label>
									<input type="text" id="name" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="field form-group col-sm-6">
									<label for="email">Email</label>
									<input type="email" id="email" required>
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="field form-group">
								<label for="message">Сообщение</label>
								<textarea id="message" rows="3" required></textarea>
								<div class="help-block with-errors"></div>
							</div>
							<button type="submit" id="form-submit" class="btn">
								Отправить
							</button>
							<div id="msgSubmit" class="text-center hidden pull-right"></div>
							<div class="clearfix"></div>
						</form>
					</div>
					<!-- CONTACT FORM WRAPPER END -->
					<div class="text-section u-w-100 copyright">
						<p class="descrip u-w-100" style="color:#fff;" >
							<span class="part">2015-2017 ООО MEDIAPANDA</span>
							<span class="part-dot"></span>
							<span class="part">Конфиденциальность.</span>
							<span class="part-dot"></span>
							<span class="part number">+7 (926) 649-49-84 <a style="color:#fff;" href="tg://resolve?domain=<ko_ko_konst>"><i style="color:#fff; font-size: 20px;" class="ion-paper-airplane"></i></a></span>
						</p>
					</div>

				</div>
				<!-- CONTACTS SECTION END -->
			</div>
			<!-- FULLPAGE WRAPPER START -->
		</div>
		<!-- WRAPPER RIGHT SECTION END -->
	</section>
	<!-- WRAPPER SECTION END -->

	<!-- JAVASCRIPT CONFIGS START-->
	<script src="<?= $u_assets_path_main ?>/js/jquery.min.js"></script>
	<script src="<?= $u_assets_path_main ?>/js/plugins.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tilt.js/1.1.21/tilt.jquery.min.js"></script>
	<script src="<?= $u_assets_path_main ?>/js/custom.js"></script>
	<!-- JAVASCRIPT CONFIGS END-->

</body>
</html>