<?php $u_assets_path_main = get_template_directory_uri() . '/assets/main'; ?>
<?php $u_assets_path_services = get_template_directory_uri() . '/assets/services'; ?>

<?php


$services = array(
	'SEO',
	'PR',
	'Контекст',
	'SMM',
	'Наружка',
	'Digital',
	'Сайты',
	'ПО',
);

$id_service = (isset($_GET['id']) && ($_GET['id'] >= 0 && $_GET['id'] <= count($services)-1)) ? (int)$_GET['id'] : 6;

?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<!-- Basic Page Needs
		================================================== -->
	<meta charset="utf-8">
	<title>Разработка сайтов Panda Media</title>
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Mobile Specific Metas
		================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!-- CSS
		================================================== --> 
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!-- Favicons
		================================================== -->
	<link rel="shortcut icon" href="<?= $u_assets_path_main ?>/img/favicon.ico">
	<!-- Included CSS files 
		================================================== -->    
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/bootstrap.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/picons.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/linea_icons.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/animate.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/main.min.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/style.min.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/base.min.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/font-style.css" id="set_font" >
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/color-black.css" id="set_color">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/elastislide.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/owl.carousel.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/owl.theme.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/owl.transitions.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/bigvideo.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/magnific-popup.css" media="screen" />
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/flexslider.min.css" media="screen" />
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/supersized.min.css" media="screen" />
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/supersized.shutter.min.css" media="screen" />

	<link rel="stylesheet" href="<?= $u_assets_path_main ?>/style/frameworks/normalize.css">

	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/main.css" />
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/modernizr.min.js"></script>
</head>
<!--     
	not_onepage_ver 		- To add this class name on body tag, It will make the page as normal type, not an one page version
	white_ver 				- To add this class name on body tag, It will identify the page as a white version
				  (It use to change the arrow, css background image as per white version Ex: To use color-white.css file)
	menuAutoClose			- For Menu autohide, just add the class name menuAutoClose on the body tag
	disableAnimation		- To disable opening animation, just add the disableAnimation class name to the body tag
	Remove Page Border		- To remove the border, just set data-removePageBorder attribute to "yes" on body tag
	-->
<body class="index-isotope menuCloseIt menuAutoClose" data-page="index" data-removePageBorder="yes" data-menuSelected="!home" >
	<!-- PRELOADER START -->
	<div class="preloader">
		<div class="loader">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
	<!-- PRELOADER START -->

	<section id="wrapper" class="wrapper">
		<div id="particles-js"></div> 
		<div class="right-side"  style="height:100%;">

			<!-- navigation start -->
			<nav class="animated fadeIn">
				<button class="menu-btn">
					<svg version="1.1" id="menuicn" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 54 33" enable-background="new 0 0 54.238 33" xml:space="preserve">
						<rect y="30" class="first" width="54.238" height="3"></rect>
						<rect x="0" y="20" class="inbetween" width="44.238" height="3"></rect>
						<rect x="0" y="10" class="inbetween" width="34.238" height="3"></rect>
						<rect width="54.238" class="last" height="3"></rect>
					</svg>
				</button>
				<ul class="">
					<li data-menuanchor="home"><a href="<?= get_site_url() ?>/#home">главная</a></li>
					<li data-menuanchor="about"><a href="<?= get_site_url() ?>/#about">о компании</a></li>
					<li data-menuanchor="skills"><a href="<?= get_site_url() ?>/#skills">навыки</a></li>
					<li data-menuanchor="services" class="active"><a href="<?= get_site_url() ?>/#services">услуги</a>
						<ul class="sub-nav show">
							<li class="<?= ($id_service == 0) ? 'active' : '' ?>"><a href="#" target="_blank">SEO</a></li>
							<li class="<?= ($id_service == 1) ? 'active' : '' ?>"><a href="#" target="_blank">PR</a></li>
							<li class="<?= ($id_service == 2) ? 'active' : '' ?>"><a href="#" target="_blank">Контекст</a></li>
							<li class="<?= ($id_service == 3) ? 'active' : '' ?>"><a href="#" target="_blank">SMM</a></li>
							<li class="<?= ($id_service == 4) ? 'active' : '' ?>"><a href="#" target="_blank">Наружка</a></li>
							<li class="<?= ($id_service == 5) ? 'active' : '' ?>"><a href="#" target="_blank">Digital</a></li>
							<li class="<?= ($id_service == 6) ? 'active' : '' ?>"><a href="#" target="_blank">Сайты</a></li>
							<li class="<?= ($id_service == 7) ? 'active' : '' ?>"><a href="#" target="_blank">ПО</a></li>
						</ul>
					</li>
					<li data-menuanchor="portfolio"><a href="<?= get_site_url() ?>/#portfolio">портфолио</a></li>
					<li data-menuanchor="news"><a href="<?= get_site_url() ?>/#news">новости</a></li>
					<li data-menuanchor="contacts"><a href="<?= get_site_url() ?>/#contacts">контакты</a></li>
				</ul>
			</nav>
			<!-- navigation end -->

			<!-- FULLPAGE WRAPPER START -->
			<div id="fullpage" class="fullpage-wrapper" style="height: 100%; position: relative; touch-action: none; transform: translate3d(0px, 0px, 0px);">

				<!-- HOME SECTION START -->
				<div class="section home fp-section active fp-table fp-completely" id="!home" data-anchor="home" style="height: 100%;">
					<div class="fp-tableCell" style="height:100%;display: flex;align-items: center;">
						<!-- text section start -->
						<div class="text-section">
							<div class="pre-title animated fadeInDown">
								Мы креативное агенство
							</div>
							<!-- text rotator start -->
							<div class="title morphext animated fadeIn">
								Разработка сайтов
							</div>
							<!-- text rotator end -->
							<p class="descrip">
								Цель — увеличение продаж и продвижение бренда
								<ul class="list">
									<li>Индивидуальный дизайн</li>
									<li>Удобная система управления контентом</li>
									<li>Хостинг (размещение) сайта</li>
									<li>Техническая поддержка</li>
								</ul>
							</p>
						</div>
						<!-- text section end -->

						<!-- social networks section start -->
						<ul class="social-section animated fadeIn">
							<li><a href="tg://resolve?domain=&lt;ko_ko_konst&gt;"><i class="ion-paper-airplane"></i></a></li>
							<li><a href="info@mediapanda.ru"><i class="ion-email"></i></a></li>
						</ul>
						<!-- social networks section end -->
					</div>
				</div>
				<!-- HOME SECTION END -->

			</div>
			<!-- FULLPAGE WRAPPER START -->
		</div>
	</section>


	<!-- Complete web site content is placed inside the bodyContainer class div except footer -->
	<div class="bodyContainer">
		<div class="mainContent" >
			<!-- About us page -->
			<div data-id="!about" class="contentWrapper" >
				<div class="pageContent addPageBorder" >
					<div class="container" > 
						<div class="row">  
							<div class="col-md-12">      	
								<div class="text-center" data-animated-in="animated fadeInUp"  data-animated-time="0"  data-animated-innerContent="yes" data-anchor-to="parent"> 
									<h4 class="mini_heading font_small dark letterSpacing center">
										Какие сайты мы делаем?
										<i class="fa fa-adn back_icon_title"></i>
									</h4>
								</div> 
							</div>
						</div>
					</div>
					<div class="fullWidth relativePos">
						<div class="container">
							<div class="row-fluid" data-animated-time="0" data-animated-in="animated fadeInLeft" data-animated-innercontent="yes" data-anchor-to="parent.parent" style="visibility: visible;">

								<div class="col-md-6 services_list2" style="visibility: visible;">
									<span class="icon">
										<i class="icon icon-basic-lightbulb fa-4x"></i>
										<i class="icon icon-basic-lightbulb fa-4x back_icon"></i>
									</span>
									<div class="desc">
										<h3>Промо-страница</h3>
										<h4 class="light_weight">от 25 000 руб</h4>
									</div>
								</div>

								<div class="col-md-6 services_list2" style="visibility: visible;">
									<span class="icon">
										<i class="icon icon-basic-heart fa-4x"></i>
										<i class="icon icon-basic-heart fa-4x back_icon"></i>
									</span>
									<div class="desc">
										<h3>Корпоративный сайт</h3>
										<h4 class="light_weight">от 80 000 руб</h4>
									</div>
								</div>
							</div>

							<div class="row-fluid" data-animated-time="2" data-animated-in="animated fadeInLeft" data-animated-innercontent="yes" data-anchor-to="parent.parent" style="visibility: visible;">

								<div class="col-md-6 services_list2" style="visibility: visible;">
									<span class="icon">
										<i class="icon icon-basic-paperplane fa-4x"></i>
										<i class="icon icon-basic-paperplane fa-4x back_icon"></i>
									</span>
									<div class="desc">
										<h3>Информационный портал</h3>
										<h4 class="light_weight">от 40 000 руб</h4>
									</div>
								</div>

								<div class="col-md-6 services_list2" style="visibility: visible;">
									<span class="icon">
										<i class="icon icon-basic-cup fa-4x"></i>
										<i class="icon icon-basic-cup fa-4x back_icon"></i>
									</span>
									<div class="desc">
										<h3>Интернет магазин</h3>
										<h4 class="light_weight">от 120 000 руб</h4>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div><!-- About us page -->
			<!-- Portfolio page -->
			<div data-id="!portfolio" class= "contentWrapper portfolioPage backGround" >
				<div class="pageContent addPageBorder" >
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="page_header noBorder text-center" data-animated-in="animated fadeInUp"  data-animated-time="0"  data-animated-innerContent="yes" data-anchor-to="parent.parent">
									<h4 class="mini_heading font_small dark letterSpacing center">
										НАШИ РАБОТЫ
										<i class="fa fa-suitcase back_icon_title"></i>
									</h4>
								</div>
							</div>
						</div>
					</div>
					<!-- Portfolio Detail content load inside the below projDetailLoad class div-->
					<div class="projDetailLoad " >
						<!-- Portfolio Next, Previous, close button and Project Number -->
						<div class="itemNav">
							<a class="close_button_pro fxEmbossBtn" > <span class="btn_icon">  <i class="icon icon-arrows-remove fa-2x"></i> </span> <span class="btn_hover"></span> </a>
							<a class="next_button_pro fxEmbossBtn" > <span class="btn_icon">  <i class="icon icon-arrows-right fa-2x"></i> </span> <span class="btn_hover"></span> </a>    		
							<a class="previous_button_pro fxEmbossBtn" > <span class="btn_icon"><i class="icon icon-arrows-left fa-2x"></i>  </span> <span class="btn_hover"></span> </a>     
							<div class="sliderNumber_pro">00/00</div>
						</div>
					</div>
					<!-- Gallery thumbnails. The thumbnail category is placed on the item class div titleShowOnHover -->
					<div class="masonry_items_holder">
						<div class="masonry_items removeImgGrayFilter titleShowOnHover portfolio_items column4 catFilterEffect_1" data-animated-time="5" data-animated-in="animated fadeIn" data-animated-innerContent="yes" data-anchor-to="parent.parent" >
							<div class="grid-sizer"></div>
							<div class="gutter-sizer"></div>
							<!-- Thumbnail -->
							<div class="item hover_enable itemOver" >
								<div class="porImgOver" >
									<!-- Thumbnail Image -->
									<img class="preload" src="http://cdn.uwebu.ru/panda/img/imperiya-sveta.jpg" alt="image_alt_text" />
								</div>
								<div class="imageText">
									<div class="imageTextHolder">
										<div class="text_field">
											<h5>Компания "Империя Света"</h5>
											<h6>Интернет-магазин</h6>
											<a class="readMore removeIcon simple detail_btn addBorder roundedRec" >
												Подробнее
												<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
											</a>
										</div>
									</div>
								</div>
								<!-- Project detail content -->
								<div class="fullDetails " >
									<div class="max_height full-width">
										<div class="col-2by3 fitToLowDevice">
											<figure>
												<a  class="lazyload alignCenter scale-fit-height" href="http://cdn.uwebu.ru/panda/img/site/imperiya-sveta.png" title="image text">Image</a>
											</figure>
										</div>
										<div class="col-1by3">
											<h3 class="title_text">Империя света</h3>
											<p>Создание интернет-магазина по продаже светильников, люстр, ламп и расходных материалов</p>
											<ul class="item_feature no_border">
												<li><span class="title"><i class="fa fa-external-link"></i></span><a href="http://imperiya-sveta.moscow" target="_blank">http://imperiya-sveta.moscow</a></li>
											</ul>
										</div>
									</div>
								</div>
								<!-- End Project detail -->
							</div>
							<div class="item hover_enable itemOver" >
								<div class="porImgOver" >
									<!-- Thumbnail Image -->
									<img class="preload" src="http://cdn.uwebu.ru/panda/img/54174895__3-e1486333845284.jpg" alt="image_alt_text" />
								</div>
								<div class="imageText">
									<div class="imageTextHolder">
										<div class="text_field">
											<h5 >Муниципальная служба дезинсекции</h5>
											<h6>Корпоративный сайт</h6>
											<a class="readMore removeIcon simple detail_btn addBorder roundedRec" >
												Подробнее
												<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
											</a>
										</div>
									</div>
								</div>
								<!-- Project detail  -->
								<div class="fullDetails " >
									<div class="max_height full-width">
										<div class="col-2by3 fitToLowDevice">
											<figure>
												<a  class="lazyload alignCenter scale-fit-height" href="http://cdn.uwebu.ru/panda/img/site/dtdpy07co5qwt37oq0bu.jpg" title="image text">Image</a>
											</figure>
										</div>
										<div class="col-1by3">
											<h3 class="title_text">Муниципальная служба дезинсекции</h3>
											<p>Сайт «Муниципальной службы дезинсекции» в Москве. Наша компания занималась созданием и полным наполнением данного сайта.</p>
											<ul class="item_feature no_border">
												<li><span class="title"><i class="fa fa-external-link"></i></span><a href="http://dezinfektsiya.moscow" target="_blank">http://dezinfektsiya.moscow</a></li>
											</ul>
										</div>
									</div>
								</div>
								<!-- End Project detail -->
							</div>
							<div class="item hover_enable itemOver" >
								<div class="porImgOver" >
									<!-- Thumbnail Image -->
									<img class="preload" src="http://cdn.uwebu.ru/panda/img/eCfKN_croper_ru-e1486510089958.jpeg" alt="image_alt_text" />
								</div>
								<div class="imageText">
									<div class="imageTextHolder">
										<div class="text_field">
											<h5>Компания "Зона шопинга"</h5>
											<h6>Интернет-магазин</h6>
											<a class="readMore removeIcon simple detail_btn addBorder roundedRec" >
												Подробнее
												<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
											</a>
										</div>
									</div>
								</div>
								<!-- Project detail -->
								<div class="fullDetails " >
									<div class="max_height full-width">
										<div class="col-2by3 fitToLowDevice">
											<figure>
												<a  class="lazyload alignCenter scale-fit-height" href="http://cdn.uwebu.ru/panda/img/site/rzvp510jpdqklll0a800.jpg" title="image text">Image</a>
											</figure>
										</div>
										<div class="col-1by3">
											<h3 class="title_text">Зона шопинга</h3>
											<p>Интернет-магазин по продаже кроссовок. Дизайн страниц и карточек товаров разрабатывался индивидуально, исходя из требований клиента.</p>
											<ul class="item_feature no_border">
												<li><span class="title"><i class="fa fa-external-link"></i></span><a href="http://zona-shopinga.com" target="_blank">http://zona-shopinga.com</a></li>
											</ul>
										</div>
									</div>
								</div>
								<!-- End Project detail -->
							</div>
							<div class="item hover_enable itemOver" >
								<div class="porImgOver" >
									<!-- Thumbnail Image -->
									<img class="preload" src="http://cdn.uwebu.ru/panda/img/go3ja_croper_ru.jpeg" alt="image_alt_text" />
								</div>
								<div class="imageText">
									<div class="imageTextHolder">
										<div class="text_field">
											<h5>Компания "Тейлоринг"</h5>
											<h6>Корпоративный</h6>
											<a class="readMore removeIcon simple detail_btn addBorder roundedRec" >
												Подробнее
												<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
											</a>
										</div>
									</div>
								</div>
								<!-- Project detail -->
								<div class="fullDetails " >
									<div class="max_height full-width">
										<div class="col-2by3 fitToLowDevice">
											<figure>
												<a  class="lazyload alignCenter scale-fit-height" href="http://cdn.uwebu.ru/panda/img/site/lgaj8enfii0sba33ynpd.jpg" title="image text">Image</a>
											</figure>
										</div>
										<div class="col-1by3">
											<h3 class="title_text">Тейлоринг</h3>
											<p>Корпоративный сайт для бизнеса по индивидуальному пошиву одежды. Дизайн был предоставлен клиентом.</p>
											<ul class="item_feature no_border">
												<li><span class="title"><i class="fa fa-external-link"></i></span><a href="http://tailoringgrup.com" target="_blank">http://tailoringgrup.com</a></li>
											</ul>
										</div>
									</div>
								</div>
								<!-- End Project detail -->
							</div>
							<div class="item hover_enable itemOver" >
								<div class="porImgOver" >
									<!-- Thumbnail Image -->
									<img class="preload" src="http://cdn.uwebu.ru/panda/img/qVGVC_croper_ru.jpeg" alt="image_alt_text" />
								</div>
								<div class="imageText">
									<div class="imageTextHolder">
										<div class="text_field">
											<h5>Техцентр "Орион"</h5>
											<h6>Корпоративный</h6>
											<a class="readMore removeIcon simple detail_btn addBorder roundedRec" >
												Подробнее
												<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
											</a>
										</div>
									</div>
								</div>
								<!-- Project detail -->
								<div class="fullDetails " >
									<div class="max_height full-width">
										<div class="col-2by3 fitToLowDevice">
											<figure>
												<a  class="lazyload alignCenter scale-fit-height" href="http://cdn.uwebu.ru/panda/img/site/yccw6yjtzz0z0eeg847v.jpg" title="image text">Image</a>
											</figure>
										</div>
										<div class="col-1by3">
											<h3 class="title_text">Мерседес</h3>
											<p>Корпоративный сайт технического центра, специализирующегося на обслуживании автомобилей марки Mercedes</p>
											<ul class="item_feature no_border">
												<li><span class="title"><i class="fa fa-external-link"></i></span><a href="http://mercedes-motors.ru" target="_blank">http://mercedes-motors.ru</a></li>
											</ul>
										</div>
									</div>
								</div>
								<!-- End Project detail -->
							</div>
							<div class="item hover_enable itemOver" >
								<div class="porImgOver" >
									<!-- Thumbnail Image -->
									<img class="preload" src="http://cdn.uwebu.ru/panda/img/D9Snu_croper_ru.jpeg" alt="image_alt_text" />
								</div>
								<div class="imageText">
									<div class="imageTextHolder">
										<div class="text_field">
											<h5>Компания "Постелика"</h5>
											<h6>Интернет-магазин</h6>
											<a class="readMore removeIcon simple detail_btn addBorder roundedRec" >
												Подробнее
												<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
											</a>
										</div>
									</div>
								</div>
								<!-- Project detail -->
								<div class="fullDetails " >
									<div class="max_height full-width">
										<div class="col-2by3 fitToLowDevice">
											<figure>
												<a  class="lazyload alignCenter scale-fit-height" href="http://cdn.uwebu.ru/panda/img/site/pwazxg6ke7syzsxf8oiy.jpg" title="image text">Image</a>
											</figure>
										</div>
										<div class="col-1by3">
											<h3 class="title_text">Постелика</h3>
											<p>Интернет-магазин для продажи различных текстильных изделий. Разработан индивидуальный дизайн и структура сайта.</p>
											<ul class="item_feature no_border">
												<li><span class="title"><i class="fa fa-external-link"></i></span><a href="http://postelnoe.moscow" target="_blank">http://postelnoe.moscow</a></li>
											</ul>
										</div>
									</div>
								</div>
								<!-- End Project detail -->
							</div>
							<div class="item hover_enable itemOver" >
								<div class="porImgOver" >
									<!-- Thumbnail Image -->
									<img class="preload" src="http://cdn.uwebu.ru/panda/img/ygTfR_croper_ru.jpeg" alt="image_alt_text" />
								</div>
								<div class="imageText">
									<div class="imageTextHolder">
										<div class="text_field">
											<h5>Компания "QP-Kart"</h5>
											<h6>Корпоративный</h6>
											<a class="readMore removeIcon simple detail_btn addBorder roundedRec" >
												Подробнее
												<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
											</a>
										</div>
									</div>
								</div>
								<!-- Project detail -->
								<div class="fullDetails " >
									<div class="max_height full-width">
										<div class="col-2by3 fitToLowDevice">
											<figure>
												<a  class="lazyload alignCenter scale-fit-height" href="http://cdn.uwebu.ru/panda/img/site/2017-02-08_14-11-01.png" title="image text">Image</a>
											</figure>
										</div>
										<div class="col-1by3">
											<h3 class="title_text">qp-kart</h3>
											<p>Сайт создан с целью &nbsp;скупки использованных картриджей. Был разработан индивидуальный дизайн &nbsp;и структура сайта.</p>
											<ul class="item_feature no_border">
												<li><span class="title"><i class="fa fa-external-link"></i></span><a href="http://qp-kart.ru" target="_blank">http://qp-kart.ru</a></li>
											</ul>
										</div>
									</div>
								</div>
								<!-- End Project detail --> 
							</div>
							<div class="item hover_enable itemOver" >
								<div class="porImgOver" >
									<!-- Thumbnail Image -->
									<img class="preload" src="http://cdn.uwebu.ru/panda/img/6nyQ0_croper_ru.jpeg" alt="image_alt_text" />
								</div>
								<div class="imageText">
									<div class="imageTextHolder">
										<div class="text_field">
											<h5>Компания "UNO ESTATE"</h5>
											<h6>Корпоративный</h6>
											<a class="readMore removeIcon simple detail_btn addBorder roundedRec" >
												Подробнее
												<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
											</a>
										</div>
									</div>
								</div>
								<!-- Project detail -->
								<div class="fullDetails " >
									<div class="max_height full-width">
										<div class="col-2by3 fitToLowDevice">
											<figure>
												<a  class="lazyload alignCenter scale-fit-height" href="http://cdn.uwebu.ru/panda/img/site/2017-02-08_14-19-32.png" title="image text">Image</a>
											</figure>
										</div>
										<div class="col-1by3">
											<h3 class="title_text">UNO ESTATE</h3>
											<p>Сайт был создан для риелторского агенства. Работа велась, в первую очередь, над главной страницей сайта.</p>
											<ul class="item_feature no_border">
												<li><span class="title"><i class="fa fa-external-link"></i></span><a href="http://sdamkvartiry.moscow" target="_blank">http://sdamkvartiry.moscow</a></li>
											</ul>
										</div>
									</div>
								</div>
								<!-- End Project detail -->
							</div>
							<div class="item hover_enable itemOver" >
								<div class="porImgOver" >
									<!-- Thumbnail Image -->
									<img class="preload" src="http://cdn.uwebu.ru/panda/img/At1If_croper_ru.png" alt="image_alt_text" />
								</div>
								<div class="imageText">
									<div class="imageTextHolder">
										<div class="text_field">
											<h5>Компания "Стартерстор"</h5>
											<h6>Интернет-магазин</h6>
											<a class="readMore removeIcon simple detail_btn addBorder roundedRec" >
												Подробнее
												<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
											</a>
										</div>
									</div>
								</div>
								<!-- Project detail -->
								<div class="fullDetails " >
									<div class="max_height full-width">
										<div class="col-2by3 fitToLowDevice">
											<figure>
												<a  class="lazyload alignCenter scale-fit-height" href="http://cdn.uwebu.ru/panda/img/site/2017-02-08_15-13-28.png" title="image text">Image</a>
											</figure>
										</div>
										<div class="col-1by3">
											<h3 class="title_text">Стартерстор</h3>
											<p>Интернет-магазин по продаже запчастей и комплектующих для грузовых и легковых автомобилей, а также иных типов транспортных средств.</p>
											<ul class="item_feature no_border">
												<li><span class="title"><i class="fa fa-external-link"></i></span><a href="http://starterstore.ru" target="_blank">http://starterstore.ru</a></li>
											</ul>
										</div>
									</div>
								</div>
								<!-- End Project detail -->
							</div>
							<div class="item hover_enable itemOver" >
								<div class="porImgOver" >
									<!-- Thumbnail Image -->
									<img class="preload" src="http://cdn.uwebu.ru/panda/img/1uQWB_croper_ru.jpeg" alt="image_alt_text" />
								</div>
								<div class="imageText">
									<div class="imageTextHolder">
										<div class="text_field">
											<h5>Строительная компания "Мимиа"</h5>
											<h6>Корпоративный</h6>
											<a class="readMore removeIcon simple detail_btn addBorder roundedRec" >
												Подробнее
												<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
											</a>
										</div>
									</div>
								</div>
								<!-- Project detail -->
								<div class="fullDetails " >
									<div class="max_height full-width">
										<div class="col-2by3 fitToLowDevice">
											<figure>
												<a  class="lazyload alignCenter scale-fit-height" href="http://cdn.uwebu.ru/panda/img/site/2017-02-08_15-57-12.png" title="image text">Image</a>
											</figure>
										</div>
										<div class="col-1by3">
											<h3 class="title_text">Мимиа</h3>
											<p>Корпоративный сайт СК Мимиа. На &nbsp;данный момент домен для сайта не еще приобретен, так как сайт находится на стадии разработки.</p>
											<ul class="item_feature no_border">
												<li><span class="title"><i class="fa fa-external-link"></i></span><a href="http://str.dmigorevich.ru" target="_blank">http://str.dmigorevich.ru</a></li>
											</ul>
										</div>
									</div>
								</div>
								<!-- End Project detail --> 
							</div>
							<div class="item hover_enable itemOver" >
								<div class="porImgOver" >
									<!-- Thumbnail Image -->
									<img class="preload" src="http://cdn.uwebu.ru/panda/img/xcT2d_croper_ru.jpeg" alt="image_alt_text" />
								</div>
								<div class="imageText">
									<div class="imageTextHolder">
										<div class="text_field">
											<h5>Спецтехника "ТТБ"</h5>
											<h6>Интернет-магаз</h6>
											<a class="readMore removeIcon simple detail_btn addBorder roundedRec" >
												Подробнее
												<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
											</a>
										</div>
									</div>
								</div>
								<!-- Project detail -->
								<div class="fullDetails " >
									<div class="max_height full-width">
										<div class="col-2by3 fitToLowDevice">
											<figure>
												<a  class="lazyload alignCenter scale-fit-height" href="http://cdn.uwebu.ru/panda/img/site/2017-02-08_16-33-02.png" title="image text">Image</a>
											</figure>
										</div>
										<div class="col-1by3">
											<h3 class="title_text">Запчасти</h3>
											<p>Интернет-магазин запчастей для спецтехники. На данный момент сайт находится на тестовом домене, так как процесс разработки еще не окончен.</p>
											<ul class="item_feature no_border">
												<li><span class="title"><i class="fa fa-external-link"></i></span><a href="http://zap.dmigorevich.ru" target="_blank">http://zap.dmigorevich.ru</a></li>
											</ul>
										</div>
									</div>
								</div>
								<!-- End Project detail -->
							</div>
							<div class="item hover_enable itemOver" >
								<div class="porImgOver" >
									<!-- Thumbnail Image -->
									<img class="preload" src="http://cdn.uwebu.ru/panda/img/cZ5Ba_croper_ru.jpeg" alt="image_alt_text" />
								</div>
								<div class="imageText">
									<div class="imageTextHolder">
										<div class="text_field">
											<h5>Центральное агенство недвижисмости</h5>
											<h6>Промо-страница</h6>
											<a class="readMore removeIcon simple detail_btn addBorder roundedRec" >
												Подробнее
												<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
											</a>
										</div>
									</div>
								</div>
								<!-- Project detail -->
								<div class="fullDetails " >
									<div class="max_height full-width">
										<div class="col-2by3 fitToLowDevice">
											<figure>
												<a  class="lazyload alignCenter scale-fit-height" href="http://cdn.uwebu.ru/panda/img/site/2017-02-08_17-06-59.png" title="image text">Image</a>
											</figure>
										</div>
										<div class="col-1by3">
											<h3 class="title_text">Центральное агенство недвижимости</h3>
											<p>Лендинг для агенства недвижимости. Основной целью создания данного сайта является привлечение новых клиентов через контекстную рекламу.</p>
											<ul class="item_feature no_border">
												<li><span class="title"><i class="fa fa-external-link"></i></span><a href="http://can-msk.ru" target="_blank">http://can-msk.ru</a></li>
											</ul>
										</div>
									</div>
								</div>
								<!-- End Project detail -->
							</div>
						</div>
					</div>
					<hr class="separator_max">
					<div class="move_up_endSection borderTop dashStyle">
						<div class="move_up mini plain"> <span> <i class="icon icon-arrows-down fa-x darkColorText"></i> </span> </div>
					</div>
				</div>
			</div>
			<!-- About2 us page -->
			<div data-id="!about2" class="contentWrapper" >
				<div class="pageContent addPageBorder" >
					<div class="container" >
						<div class="row">
							<div class="col-md-12">
								<div class="page_header text-center" data-animated-in="animated fadeInUp"  data-animated-time="0"  data-animated-innerContent="yes" data-anchor-to="parent">
									<h4 class="mini_heading font_small dark letterSpacing center">
										Наш подход к разработке сайтов
										<i class="fa fa-adn back_icon_title"></i>
									</h4>
									<hr>
								</div>
							</div>
						</div>
						<div class="row"  data-animated-in="animated fadeInLeft"  data-animated-time="0"  data-animated-innerContent="yes" data-anchor-to="parent.parent.parent">
							<div  class="col-md-5" >
								<!-- Tab navigation -->
								<ul class="nav nav-tabs bottomBorder"  >
									<li class="active"><a href="index_slideshow.html#tab11" data-toggle="tab">Прототипирование</a></li>
									<li><a href="index_slideshow.html#tab12" data-toggle="tab">Пользовательские сценарии</a></li>
									<li><a href="index_slideshow.html#tab13" data-toggle="tab">Дизайн</a></li>
									<li><a href="index_slideshow.html#tab14" data-toggle="tab">Разработка</a></li>
									<li><a href="index_slideshow.html#tab15" data-toggle="tab">Тех. поддержка</a></li>
								</ul>
								<!-- Tab content -->
								<div class="tab-content">
									<div class="tab-pane active" id="tab11">
										<div  class="col-md-12">
											<div class="services_list2">
												<hr>
												<!-- Tab content -->
												<span class="icon">
												<i class="icon fa fa-bank fa-3x light_color"></i>
												</span>
												<div class="desc">
													<p>Разработка прототипа сайта необходима для того, чтобы заказчик и разработчик имели чёткое представление об особенностях проекта, находились в одном информационном поле.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="tab12">
										<div  class="col-md-12">
											<div class="services_list2">
												<hr>
												<!-- Tab content -->
												<span class="icon">
												<i class="icon fa fa-steam fa-3x light_color"></i>
												</span>
												<div class="desc">
													<p>Целью взаимодействия посетителя с сайтом может быть контакт с компанией, покупка в интернет-магазине, регистрация или другое действие. Мы моделируем сценарии поведения на сайте для различных групп пользователей, планируем навигацию и представление информации таким образом, чтобы сайт приводил пользователей к запланированной цели.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="tab13">
										<div  class="col-md-12">
											<div class="services_list2">
												<hr>
												<!-- Tab content -->
												<span class="icon">
												<i class="icon fa fa-binoculars fa-3x light_color"></i>
												</span>
												<div class="desc">
													<p>Качественный дизайн сайта - это гораздо больше, чем красивая и оригинальная картинка. Он должен разговаривать с целевой аудиторией, акцентировать преимущества ваших предложений, помогать посетителям получить нужную информацию и принять решение о заказе.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="tab14">
										<div  class="col-md-12">
											<div class="services_list2">
												<hr>
												<!-- Tab content -->
												<span class="icon">
												<i class="icon fa fa-binoculars fa-3x light_color"></i>
												</span>
												<div class="desc">
													<p>Мы разрабатываем сайт итерационно, с частыми демонстрациями того, что получается. Подобный подход позволяет клиенту четко понимать, на какой стадии выполнения находится сейчас та или иная задача. Важная составляющая раздела «Разработка» – работа с программным кодом, включающая в себя его написание, тестирование, отладку, а также проверку верстки на кросс-браузерность и адаптивность под мобильные устройства.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="tab15">
										<div  class="col-md-12">
											<div class="services_list2">
												<hr>
												<!-- Tab content -->
												<span class="icon">
												<i class="icon fa fa-binoculars fa-3x light_color"></i>
												</span>
												<div class="desc">
													<p>Мы делаем такие сайты, которые позволяют владельцам самостоятельно управлять ими без привлечения специалистов. Мы можем взять на себя все работы по текущей эксплуатации сайта (например, обновление информации) и по его развитию.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div  class="col-md-7" >
								<img alt="image_alt_text" class="preload scale-with-grid alignCenter" src="<?= $u_assets_path_services ?>/images/about_top_img.png" data-src="<?= $u_assets_path_services ?>/images/about_top_img.png" > 
								<hr>
							</div>
						</div>
						<hr class="separator_mini onlyHigDevice">
					</div>
				</div>
			</div>
			<!-- About3 us page -->
			<div data-id="!about3" class="contentWrapper enablHardwareAcc">
				<div class="pageContent addPageBorder">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="page_header text-center" data-animated-in="animated fadeInUp" data-animated-time="0" data-animated-innercontent="yes" data-anchor-to="parent" style="visibility: visible;">
									<h4 class="mini_heading font_small dark letterSpacing center" style="visibility: visible;">
										ПРЕИМУЩЕСТВА ЗАКАЗА САЙТА У НАС
										<i class="fa fa-adn back_icon_title"></i>
									</h4>
								</div>
							</div>
						</div>
						<div class="row" data-animated-in="animated fadeInLeft" data-animated-time="0" data-animated-innercontent="yes" data-anchor-to="parent" style="visibility: visible;">
							<div class="col-md-3 services_list1" style="visibility: visible;">
								<span class="icon">
								<i class="icon fa fa-bank fa-3x light_color"></i>
								</span>
								<div class="desc">
									<h5>Уникальный, не шаблонный сайт</h5>
									<p>Cоздание сайта мы рассматриваем как творческий процесс, в котором все подчинено определенным бизнес-задачам. Вы получите уникальный ресурс, разработанный в соответствии с вашими требованиями.</p>
								</div>
								<hr>
							</div>
							<div class="col-md-3 services_list1" style="visibility: visible;">
								<span class="icon">
								<i class="icon fa fa-steam fa-3x light_color"></i>
								</span>
								<div class="desc">
									<h5>Первичная SEO-оптимизация</h5>
									<p>Будучи seo-мастерами, мы точно знаем все нюансы и тонкости продвижения, которые обязательно учитываются при разработке сайта. Вам не нужно будет переплачивать за доработки при последующем сео-продвижении.</p>
								</div>
								<hr>
							</div>
							<div class="col-md-3 services_list1" style="visibility: visible;">
								<span class="icon">
								<i class="icon fa fa-binoculars fa-3x light_color"></i>
								</span>
								<div class="desc">
									<h5>Технологичность и широкие возможности</h5>
									<p>Мы располагаем самым современным функционалом, знаем и применяем самые современные маркетинговые, дизайнерские, программные «фишки».</p>
								</div>
								<hr>
							</div>
							<div class="col-md-3 services_list1" style="visibility: visible;">
								<span class="icon">
								<i class="icon fa fa-binoculars fa-3x light_color"></i>
								</span>
								<div class="desc">
									<h5>Гарантированное качество продукта</h5>
									<p>Оптимальный баланс скорости, качества и стоимости разработки сайта. Мы работаем на результат и делаем высокое качество услуг по созданию интернет-ресурса доступным.</p>
								</div>
								<hr>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Contact us page -->
			<div data-id="!contactus" class="contentWrapper" >
				<div class="pageContent addPageBorder" >
					<div class="container" data-animated-time="0" data-animated-in="animated fadeInUp"  data-animated-innerContent="yes" data-anchor-to="parent.parent" >
						<div class="row">
							<div class="col-md-12">
								<div class="page_header noBorder text-center">
									<h4 class="mini_heading font_small dark letterSpacing center">
										Сколько стоит сделать сайт
										<i class="fa fa-coffee back_icon_title"></i>
									</h4>
									<p class="pad_large_xx font_small onlyHigDevice padTop_0 padBot_0" style="font-weight: 100;">
										<span class="pad_large padBot_0">
										Для понимания точной стоимости и сроков разработки сайта необходим индивидуальный подход к оценке каждого проекта. Стоимость и сроки сильно зависят от задач бизнеса, типа сайта и посадочных страниц, количества подключаемых модулей (галерея, вопрос-ответ, видеовставки, каталог и т.д.). Заполните форму, и наши специалисты свяжутся с вами для обсуждения необходимых деталей.
										</span>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="row-fluid" data-animated-time="1" data-animated-in="animated fadeInUp" data-animated-innerContent="yes"  data-anchor-to="parent.parent.parent" >
							<div class="col-md-5" >
								<div class="tag_leftTitle">
									<span class="icon"><i class="icon-basic-map"></i></span>                    
									<h5 class="light_weight">Россия, г. Москва, ул.</h5>
									<h5 class="light_weight">Щербаковская 3. оф.704</h5>
								</div>
								<div class="tag_leftTitle">
									<span class="icon"><i class="icon-basic-smartphone"></i></span>                    
									<h5 >Телефон</h5>
									<h5 class="light_weight">+7 (926) 649-49-84</h5>
								</div>
								<div class="tag_leftTitle">
									<span class="icon"><i class="icon-basic-paperplane"></i></span> 
									<h5 >Телеграм</h5>
									<h5 class="light_weight">+7 (926) 649-49-84</h5>
								</div>
								<div class="tag_leftTitle">
									<span class="icon"><i class="icon-basic-mail-open"></i></span>                    
									<h5 >Отправить письмо</h5>
									<h5 class="light_weight"><a>info@mediapanda.ru</a></h5>
								</div>
								<br>
								<hr class="separator_mini">
							</div>
							<div class="col-md-7" >
								<h4 class="font_medium_x left_spacing">Напишите нам</h4>
								<hr class="separator_mini">
								<!--Email contact Form-->
								<form name="contact" class="contactusForm" method="post" >
									<div class="row-fluid" >
										<div class="col-md-6">
											<input type="text" value="Имя" id="name" name="name" class="col-md-12 transprentBg dottedBorder" 
												onfocus="if(this.value == 'Имя') {this.value = '';}"	
												onblur="if (this.value == '') {this.value = 'Имя';}" />
										</div>
										<div class="col-md-6">
											<input type="text" value="Email" id="email" name="email" class="col-md-12 transprentBg dottedBorder"  
												onfocus="if(this.value == 'Email') {this.value = '';}"  
												onblur="if (this.value == '') {this.value = 'Email';}" />
										</div>
									</div>
									<div class="row-fluid" >
										<div class="col-md-6">
											<input type="text" value="Ваш сайт" id="phone" name="phone" class="col-md-12 transprentBg dottedBorder" 
												onfocus="if(this.value == 'Ваш сайт') {this.value = '';}"	
												onblur="if (this.value == '') {this.value = 'Ваш сайт';}" />
										</div>
										<div class="col-md-6">
											<input type="text" value="Телефон" id="phone" name="phone" class="col-md-12 transprentBg dottedBorder"  
												onfocus="if(this.value == 'Телефон') {this.value = '';}"  
												onblur="if (this.value == '') {this.value = 'Телефон';}" />
										</div>
									</div>
									<div class="row-fluid">
										<div class="col-md-12">
											<textarea cols="30" rows="5" id="message" name="message" class="col-md-12 transprentBg dottedBorder"	
												onfocus="if(this.value == 'Сообщение...') {this.value = '';}" 
												onblur="if (this.value == '') {this.value = 'Сообщение...';}">Сообщение...</textarea>        
										</div>
									</div>
									<hr>
									<div class="row-fluid">
										<div class="col-md-12 textAlignCenter alignCenter">
											<button type="submit" id="email_submit"  class="button large transprentBg dashedBorder makeAnimate"> 
											Отправить
											</button>
											<div id="reply_message" ></div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="fullWidth map_holder greyBackground relativePos borderTop textAlignCenter">
						<!-- <hr class="space_mini"> -->
						<!-- <a class="readMore removeIcon simple center addBorder roundedRec openGoogleMap light_weight hideMap dark" >  -->
							<!-- <span class="loadMap">Открыть карту</span>   -->
							<!-- <span class="removeMap">view our work place</span> -->
						<!-- </a> -->
						<hr class="space_mini">
						 <!-- <div id="map_canvas" class="pointerCenter removebackground autoLoadOff parallax" data-src="<?= $u_assets_path_services ?>/images/background/image5_s.jpg"  data-src-small="<?= $u_assets_path_services ?>/images/background/image5_s.jpg" ></div> -->
						 <iframe id="map_canvas" class="pointerCenter removebackground autoLoadOff parallax" src="https://www.google.ru/maps/d/embed?mid=1pwxDv64i0sSaynusIqfWgR_PCXY"></iframe>
					</div>
				</div>
			</div>
			<!-- contactus page -->
			<div class="fullWidth relativePos" >
				<div class="container-fluid top_bot_pad">
					<div class="row-fluid">
						<div class="col-md-12 text-center" >
							<hr class="separator_mini">
							<h5 class="light_weight tiny_font">2015 - 2017 ООО MEDIAPANDA</h5>
							<h6 class="fontFamily_1">Конфиденциальность</h6>
						</div>
					</div>
				</div>
				<hr class="separator_mini">
			</div>
		</div>
	</div>

	<!-- Included javascript files 
		================================================== -->
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery-1.11.0.min.js"></script> 
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/SmoothScroll.js"></script> 
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.transit.js"></script>    
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.browser.js"></script>    
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/bootstrap.min.js"></script> 
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.touchSwipe.min.js"></script>     
	<!--   	    --> 
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jflickrfeed.min.js"></script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBLJqvcIfq3VDtarh0ykxPgeuHXn-sgC5E"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/supersized.3.2.7.min.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/supersized.shutter.min.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.fitvids.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.nicescroll.min.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquerypp.elastislide.custom.js" ></script>     
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.elastislide.js" ></script>  
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/owl.carousel.min.js" ></script>      
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.cycle.all.js"></script>   
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/portfolio.detail.min.js"></script>  
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.flexslider.min.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jQuery.tubeplayer.js"></script>    
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/isotope-docs.js"></script>    
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.magnific-popup.min.js"></script> 
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.support.plugin.min.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/custom.min.js"></script> 
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/video.min.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/bigvideo.js"></script> 
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/page_default.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/main-fm.min.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/main.js"></script>
</body>
</html>