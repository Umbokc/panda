<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package panda-theme
 */

?>

<section>

	<div class="panda_post_block">
		<ul>
			<li><?php the_time('M d, Y'); ?></li>
			<li>by <?php the_author(); ?></li>
			<li><?php the_tags('in ', ' + ') ?></li>
		</ul>
		<?php the_title( '<h1>', '</h1>' ); ?>
		<?php 
			if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
				echo '<img src="';
				the_post_thumbnail_url('full');
				echo '" alt="">';
			}
		?>
		<?php // my_wpautop(); ?>	
		<?php the_content(); ?>
	</div>
	<ul class="panda_posts_button clearfix">
		<li><?php the_tags('', '') ?></li>
		<li class="panda_sharing"><a class="panda_sharing_button" href="javascript:void(0)">Поделиться</a>
			<div class="panda_sharing_popup">

				<div class="panda_sharing_popup_overlay"></div>
				<div class="panda_sharing_popup_close"></div>

				<div class="panda_sharing_url_cont">
					<div class="panda_sharing_url_title">
						<div class="panda_title_default">Поделиться этим постом</div>
						<div class="panda_title_copied">Ваша ссылка скопирована</div>
					</div>
					<div class="panda_sharing_form">
						<input class="panda_sharing_url" type="text" name="sharing_link" value="https://http://pixel-mafia.com/demo/wordpress-themes/panda/">
						<button class="panda_sharing_url_button">
							<i class="fa fa-link"></i>
						</button>
					</div>
				</div>
				<a class="panda_share_facebook" href="#">
					<i class="fa fa-facebook"></i>
				</a>

				<a class="panda_share_twitter" href="#">
					<i class="fa fa-twitter"></i>
				</a>

				<a class="panda_share_google_plus" href="#">
					<i class="fa fa-google-plus"></i>
				</a>

				<a class="panda_share_pinterest" href="#">
					<i class="fa fa-pinterest"></i>
				</a>

				<a class="panda_share_vk" href="#">
					<i class="fa fa-vk"></i>
				</a>

				<a class="panda_share_tumblr" href="#">
					<i class="fa fa-tumblr"></i>
				</a>
			</div>
		</li>
	</ul>
</section>

<div class="panda_pagination">
	<ul>
		<?php previous_post_link(
			'<li>%link</li>', '<i class="fa fa-arrow-left" aria-hidden="true"></i> Предыдущая статья'
		); ?> 
		<?php next_post_link(
			'<li>%link</li>', 'Следующая статья <i class="fa fa-arrow-right" aria-hidden="true"></i>'
		); ?> 
	</ul>
</div>


<article class="panda_related_posts clearfix">
	<h5>Вам также может понравиться</h5>
	<div class="row">
		<?php
		global $post;
		$args = array('posts_per_page' => 5, 'offset'=> 1, 'exclude' => get_the_ID());
		$myposts = get_posts( $args );
		$i = 0; 
		foreach( $myposts as $post ){ setup_postdata($post); if ( !has_post_thumbnail() ) continue;
			if($i >= 2) break; ++$i;
			?>
			<div  class="col-md-6">
				<img width=371 height=300 style="object-fit: cover;" src="<?php the_post_thumbnail_url('full');?>" alt="">
				<time datetime="<?php the_time('Y-m-d'); ?>"><?php the_time('M d, Y'); ?></time>
				<a href="<?= get_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
				<p class="panda_text"><?php the_excerpt(); ?></p>
			</div>
			<?php
		}
		wp_reset_postdata();
		?>
	</div>
</article>



<?php 
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;
?>

<article class="panda_contact_form">
<?php 
comment_form(array(
	'comment_field' => '<label for="comment"></label><input type="text" name="comment" class="panda_contact_join" value="' . esc_attr( $commenter['comment'] ) . '"placeholder="Ваше сообщение">',
	'fields' => array(
		'author' => '<label for="author"></label><input type="text" name="author" value="' . esc_attr( $commenter['author'] ) . '" placeholder="Введите ваше имя *">',
		'email'  => '<label for="email"></label><input type="text" name="email" value="' . esc_attr(  $commenter['email'] ) . '" placeholder="Введите ваш email *">',
	),
	'title_reply'          => __( 'Добавить коментарий' ),
	'title_reply_to'       => __( 'Ответ на коментарий "%s"' ),
	'title_reply_before'   => '<h5 id="reply-title" class="comment-reply-title">',
	'title_reply_after'    => '</h5>',
	'comment_notes_before' => '<p class="comment-notes"><span id="email-notes">' . __( '' ) . '</span>'. ( $req ? $required_text : '' ) . '</p>',
	'comment_notes_after'  => '',
	'submit_button' => '<button class="panda_reference" name="%1$s" type="submit" id="%2$s" >%4$s</button>'
));
?>
</article>