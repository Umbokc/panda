<?php $u_assets_path_blog = get_template_directory_uri() . '/assets/blog'; ?>
<aside class="col-md-3 panda_sidebar">
	<?php // get_sidebar(); ?>

	<article class="panda_about_sidebar">
		<h6>Блог</h6>
		<p class="panda_text">Здесь мы публикуем интересные новости о наших проектах или актуальные статьи из индустрии продвижения и диджитал-маркетинга. </p>
		<p class="panda_text">Подписывайтесь на нашу почтовую рассылку и будьте в курсе.</p>
	</article>

	<?php get_search_form(); ?>
	
	<article class="panda_categories">
		<h6>Услуги</h6>
		<ul>
			<?php 
				// $categories = get_categories();
				// if( $categories ){
				// 	foreach( $categories as $cat ):{
				// 		echo '<li><a href="'.esc_url(get_category_link( $cat->cat_ID )).'">'.
				// 					$cat->name .'</a><span>('. wt_get_category_count($cat->cat_ID) .')</span></li>';
				// 	}
				// }
			?>
			
			<li><a href="#">SEO</a></li>
			<li><a href="#">PR</a></li>
			<li><a href="#">Контекст</a></li>
			<li><a href="#">SMM</a></li>
			<li><a href="#">Наружная реклама</a></li>
			<li><a href="#">Digital</a></li>
			<li><a href="#">Разработка сайтов</a></li>
			<li><a href="#">Разработка ПО</a></li>
		</ul>
	</article>
	<article class="panda_posts">
		<h6>Интересное</h6>
		<ul>
			<?php
			global $post;
			$args = array('posts_per_page' => 5, 'offset'=> 1, 'exclude' => get_the_ID());
			$myposts = get_posts( $args );
			$i = 0; 
			foreach( $myposts as $post ){ setup_postdata($post); if ( !has_post_thumbnail() ) continue;
				if($i >= 2) break; ++$i;
				?>
					<li>
						<a href="<?= get_permalink(); ?>">
							<img width=100 height=75 style="object-fit: cover;" src="<?php the_post_thumbnail_url(array(50, 50));?>" alt="">
						</a>
						<a href="<?= get_permalink(); ?>"><p ><?php the_title(); ?></p></a>
						<time datetime="<?php the_time('Y-m-d'); ?>"><?php the_time('M d, Y'); ?></time>
						<div class="clear"></div>
					</li>
				<?php
			}
			wp_reset_postdata();
			?>
		
		</ul>
	</article>
	<article class="panda_tags">
		<h6>Тэги</h6>
		<ul>
			<?php 
				$tags = get_tags();
				foreach ($tags as $tag){
					$tag_link = get_tag_link($tag->term_id);

					$html .= "<li><a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
					$html .= "{$tag->name}</a></li>";
				}
				echo $html;
			?>
		</ul>
	</article>
	<article class="panda_banner">
		<img src="<?= $u_assets_path_blog ?>/img/banner_img.jpg" alt="">
	</article>
</aside>