<section>
	<div class="panda_post_block">
		<ul>
			<li><?php the_time('M d, Y'); ?></li>
			<li>Автор: <?php the_author(); ?></li>
			<li><?php the_tags('in ', ' + ') ?></li>
		</ul>
		<?php the_title( '<h2>', '</h2>' ); ?>
		<?php 
		if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
			echo '<img src="';
			the_post_thumbnail_url('full');
			echo '" alt="">';
		}
		?>
		<p class="panda_text">
		<?php the_excerpt(); ?>
		</p>
		<a class="panda_reference" href="<?= get_permalink(); ?>">Читать далее</a>
	</div>

</section>