<?php $u_template_dir = get_template_directory_uri(); ?>
<?php $u_site_url = get_site_url(); ?>
<?php $u_assets_path_main = $u_template_dir . '/assets/main'; ?>
<?php $u_assets_path_services = $u_template_dir . '/assets/services'; ?>

<?php
$id_service = 6;
$title_site = 'Разработка сайтов Panda Media';
$services_parts_text_section_content = '
<div class="pre-title animated fadeInDown">
	Мы креативное агенство
</div>
<!-- text rotator start -->
<div class="title morphext animated fadeIn">
	Разработка сайтов
</div>
<!-- text rotator end -->
<p class="descrip">
	Цель — увеличение продаж и продвижение бренда
	<ul class="list">
		<li>Индивидуальный дизайн</li>
		<li>Удобная система управления контентом</li>
		<li>Хостинг (размещение) сайта</li>
		<li>Техническая поддержка</li>
	</ul>
</p>
';
require_once 'services-parts/header.php'; ?>

<!-- About us page -->
<div data-id="!about" class="contentWrapper">
	<div class="pageContent addPageBorder u-js-style-h-auto!" >
		<div class="container" > 
			<div class="row">  
				<div class="col-md-12">      	
					<div class="text-center" data-animated-in="animated fadeInUp"  data-animated-time="0"  data-animated-innerContent="yes" data-anchor-to="parent"> 
						<h4 class="mini_heading font_small dark letterSpacing center">
							Какие сайты мы делаем?
							<i class="fa fa-adn back_icon_title"></i>
						</h4>
					</div> 
				</div>
			</div>
			<hr class="separator_max">
		</div>
		<div class="fullWidth relativePos">
			<div class="container">
				<div class="row-fluid" data-animated-time="0" data-animated-in="animated fadeInLeft" data-animated-innercontent="yes" data-anchor-to="parent.parent" style="visibility: visible;">

					<div class="col-md-6 services_list2" style="visibility: visible;">
						<span class="icon">
							<i class="icon icon-basic-lightbulb fa-4x"></i>
							<i class="icon icon-basic-lightbulb fa-4x back_icon"></i>
						</span>
						<div class="desc">
							<h3>Промо-страница</h3>
							<h4 class="light_weight">от 25 000 руб</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, voluptas officia, deleniti in nisi nam est assumenda, possimus consectetur temporibus minima voluptatibus reprehenderit soluta natus nesciunt eaque quod veniam velit.</p>
						</div>
					</div>

					<div class="col-md-6 services_list2" style="visibility: visible;">
						<span class="icon">
							<i class="icon icon-basic-heart fa-4x"></i>
							<i class="icon icon-basic-heart fa-4x back_icon"></i>
						</span>
						<div class="desc">
							<h3>Корпоративный сайт</h3>
							<h4 class="light_weight">от 80 000 руб</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est pariatur temporibus nulla, sit optio non quo tenetur, rem consequatur enim sequi magnam veritatis fugiat labore, atque iste. Perferendis libero, accusamus!</p>
						</div>
					</div>
				</div>

				<div class="row-fluid" data-animated-time="2" data-animated-in="animated fadeInLeft" data-animated-innercontent="yes" data-anchor-to="parent.parent" style="visibility: visible;">

					<div class="col-md-6 services_list2" style="visibility: visible;">
						<span class="icon">
							<i class="icon icon-basic-paperplane fa-4x"></i>
							<i class="icon icon-basic-paperplane fa-4x back_icon"></i>
						</span>
						<div class="desc">
							<h3>Информационный портал</h3>
							<h4 class="light_weight">от 40 000 руб</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio facilis nam laudantium eligendi hic, iure repellendus amet non dolores ducimus vitae, mollitia sequi quae, animi blanditiis harum. At pariatur, nihil.</p>
						</div>
					</div>

					<div class="col-md-6 services_list2" style="visibility: visible;">
						<span class="icon">
							<i class="icon icon-basic-cup fa-4x"></i>
							<i class="icon icon-basic-cup fa-4x back_icon"></i>
						</span>
						<div class="desc">
							<h3>Интернет магазин</h3>
							<h4 class="light_weight">от 120 000 руб</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et molestias dolor ipsa quos blanditiis at deserunt, ratione quo repellat ea dolorem ipsum distinctio nisi fugiat quaerat dolorum assumenda magni amet.</p>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div><!-- About us page -->
<!-- Portfolio page -->
<div data-id="!portfolio" class= "contentWrapper portfolioPage backGround" >
	<div class="pageContent addPageBorder" >
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="page_header noBorder text-center" data-animated-in="animated fadeInUp"  data-animated-time="0"  data-animated-innerContent="yes" data-anchor-to="parent.parent">
						<h4 class="mini_heading font_small dark letterSpacing center">
							НАШИ РАБОТЫ
							<i class="fa fa-suitcase back_icon_title"></i>
						</h4>
					</div>
				</div>
			</div>
		</div>
		<!-- Portfolio Detail content load inside the below projDetailLoad class div-->
		<div class="projDetailLoad" >
			<!-- Portfolio Next, Previous, close button and Project Number -->
			<div class="itemNav">
				<a class="close_button_pro fxEmbossBtn" > <span class="btn_icon">  <i class="icon icon-arrows-remove fa-2x"></i> </span> <span class="btn_hover"></span> </a>
				<a class="next_button_pro fxEmbossBtn" > <span class="btn_icon">  <i class="icon icon-arrows-right fa-2x"></i> </span> <span class="btn_hover"></span> </a>    		
				<a class="previous_button_pro fxEmbossBtn" > <span class="btn_icon"><i class="icon icon-arrows-left fa-2x"></i>  </span> <span class="btn_hover"></span> </a>     
				<div class="sliderNumber_pro">00/00</div>
			</div>
		</div>
		
		<!-- Gallery thumbnails. The thumbnail category is placed on the item class div titleShowOnHover -->
		<div class="masonry_items_holder container alignCenter">
			<div class="masonry_items removeImgGrayFilter titleShowOnHover portfolio_items catFilterEffect_1 ajaxPopup_gallery" data-animated-time="5" data-animated-in="animated fadeIn" data-animated-innerContent="yes" data-anchor-to="parent.parent" >

				<div class="grid-sizer"></div>
				<div class="gutter-sizer"></div>

				<!-- Thumbnail -->

				<div class="item hover_enable itemOver selPopup" >
					<div class="porImgOver" >                    
						<!-- Thumbnail Image -->
						<img class="preload" src="http://cdn.uwebu.ru/panda/img/imperiya-sveta.jpg" alt="image_alt_text" />
					</div> 
					<div class="imageText"> 
						<div class="imageTextHolder">
							<div class="text_field">
								<h5>Компания "Империя Света"</h5>
								<h6>Интернет-магазин</h6>
								<a class="readMore removeIcon simple detail_btn addBorder roundedRec ajaxPopup_galItem"  href="<?= $u_template_dir ?>/parts/popup-potfolio-content.php" 
									u-mfp-title="Империя света"
									u-mfp-image="http://cdn.uwebu.ru/panda/img/site/imperiya-sveta.png"
									u-mfp-desc="Создание интернет-магазина по продаже светильников, люстр, ламп и расходных материалов"
									u-mfp-site="http://imperiya-sveta.moscow"
									>
									Подробнее
									<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
								</a> 
							</div>
						</div>                        
					</div>
				</div>
				<div class="item hover_enable itemOver selPopup" >
					<div class="porImgOver" >                    
						<!-- Thumbnail Image -->
						<img class="preload" src="http://cdn.uwebu.ru/panda/img/54174895__3-e1486333845284.jpg" alt="image_alt_text" />
					</div> 
					<div class="imageText"> 
						<div class="imageTextHolder">
							<div class="text_field">
								<h5>Муниципальная служба дезинсекции</h5>
								<h6>Корпоративный сайт</h6>
								<a class="readMore removeIcon simple detail_btn addBorder roundedRec ajaxPopup_galItem"  href="<?= $u_template_dir ?>/parts/popup-potfolio-content.php" 
									u-mfp-title="Муниципальная служба дезинсекции"
									u-mfp-image="http://cdn.uwebu.ru/panda/img/site/dtdpy07co5qwt37oq0bu.jpg"
									u-mfp-desc="Сайт «Муниципальной службы дезинсекции» в Москве. Наша компания занималась созданием и полным наполнением данного сайта."
									u-mfp-site="http://dezinfektsiya.moscow"
									>
									Подробнее
									<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
								</a> 
							</div>
						</div>                        
					</div>
				</div>
				<div class="item hover_enable itemOver selPopup" >
					<div class="porImgOver" >                    
						<!-- Thumbnail Image -->
						<img class="preload" src="http://cdn.uwebu.ru/panda/img/eCfKN_croper_ru-e1486510089958.jpeg" alt="image_alt_text" />
					</div> 
					<div class="imageText"> 
						<div class="imageTextHolder">
							<div class="text_field">
								<h5>Компания "Зона шопинга"</h5>
								<h6>Интернет-магазин</h6>
								<a class="readMore removeIcon simple detail_btn addBorder roundedRec ajaxPopup_galItem"  href="<?= $u_template_dir ?>/parts/popup-potfolio-content.php" 
									u-mfp-title="Зона шопинга"
									u-mfp-image="http://cdn.uwebu.ru/panda/img/site/rzvp510jpdqklll0a800.jpg"
									u-mfp-desc="Интернет-магазин по продаже кроссовок. Дизайн страниц и карточек товаров разрабатывался индивидуально, исходя из требований клиента."
									u-mfp-site="http://zona-shopinga.com"
									>
									Подробнее
									<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
								</a> 
							</div>
						</div>                        
					</div>
				</div>
				<div class="item hover_enable itemOver selPopup" >
					<div class="porImgOver" >                    
						<!-- Thumbnail Image -->
						<img class="preload" src="http://cdn.uwebu.ru/panda/img/go3ja_croper_ru.jpeg" alt="image_alt_text" />
					</div> 
					<div class="imageText"> 
						<div class="imageTextHolder">
							<div class="text_field">
								<h5>Компания "Тейлоринг"</h5>
								<h6>Корпоративный</h6>
								<a class="readMore removeIcon simple detail_btn addBorder roundedRec ajaxPopup_galItem"  href="<?= $u_template_dir ?>/parts/popup-potfolio-content.php" 
									u-mfp-title="Тейлоринг"
									u-mfp-image="http://cdn.uwebu.ru/panda/img/site/lgaj8enfii0sba33ynpd.jpg"
									u-mfp-desc="Корпоративный сайт для бизнеса по индивидуальному пошиву одежды. Дизайн был предоставлен клиентом."
									u-mfp-site="http://tailoringgrup.com"
									>
									Подробнее
									<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
								</a> 
							</div>
						</div>                        
					</div>
				</div>
				<div class="item hover_enable itemOver selPopup" >
					<div class="porImgOver" >                    
						<!-- Thumbnail Image -->
						<img class="preload" src="http://cdn.uwebu.ru/panda/img/qVGVC_croper_ru.jpeg" alt="image_alt_text" />
					</div> 
					<div class="imageText"> 
						<div class="imageTextHolder">
							<div class="text_field">
								<h5>Техцентр "Орион"</h5>
								<h6>Корпоративный</h6>
								<a class="readMore removeIcon simple detail_btn addBorder roundedRec ajaxPopup_galItem"  href="<?= $u_template_dir ?>/parts/popup-potfolio-content.php" 
									u-mfp-title="Мерседес"
									u-mfp-image="http://cdn.uwebu.ru/panda/img/site/yccw6yjtzz0z0eeg847v.jpg"
									u-mfp-desc="Корпоративный сайт технического центра, специализирующегося на обслуживании автомобилей марки Mercedes"
									u-mfp-site="http://mercedes-motors.ru"
									>
									Подробнее
									<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
								</a> 
							</div>
						</div>                        
					</div>
				</div>
				<div class="item hover_enable itemOver selPopup" >
					<div class="porImgOver" >                    
						<!-- Thumbnail Image -->
						<img class="preload" src="http://cdn.uwebu.ru/panda/img/D9Snu_croper_ru.jpeg" alt="image_alt_text" />
					</div> 
					<div class="imageText"> 
						<div class="imageTextHolder">
							<div class="text_field">
								<h5>Компания "Постелика"</h5>
								<h6>Интернет-магазин</h6>
								<a class="readMore removeIcon simple detail_btn addBorder roundedRec ajaxPopup_galItem"  href="<?= $u_template_dir ?>/parts/popup-potfolio-content.php" 
									u-mfp-title="Постелика"
									u-mfp-image="http://cdn.uwebu.ru/panda/img/site/pwazxg6ke7syzsxf8oiy.jpg"
									u-mfp-desc="Интернет-магазин для продажи различных текстильных изделий. Разработан индивидуальный дизайн и структура сайта."
									u-mfp-site="http://postelnoe.moscow"
									>
									Подробнее
									<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
								</a> 
							</div>
						</div>                        
					</div>
				</div>
				<div class="item hover_enable itemOver selPopup" >
					<div class="porImgOver" >                    
						<!-- Thumbnail Image -->
						<img class="preload" src="http://cdn.uwebu.ru/panda/img/ygTfR_croper_ru.jpeg" alt="image_alt_text" />
					</div> 
					<div class="imageText"> 
						<div class="imageTextHolder">
							<div class="text_field">
								<h5>Компания "QP-Kart"</h5>
								<h6>Корпоративный</h6>
								<a class="readMore removeIcon simple detail_btn addBorder roundedRec ajaxPopup_galItem"  href="<?= $u_template_dir ?>/parts/popup-potfolio-content.php" 
									u-mfp-title="qp-kart"
									u-mfp-image="http://cdn.uwebu.ru/panda/img/site/2017-02-08_14-11-01.png"
									u-mfp-desc="Сайт создан с целью &nbsp;скупки использованных картриджей. Был разработан индивидуальный дизайн &nbsp;и структура сайта."
									u-mfp-site="http://qp-kart.ru"
									>
									Подробнее
									<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
								</a> 
							</div>
						</div>                        
					</div>
				</div>
				<div class="item hover_enable itemOver selPopup" >
					<div class="porImgOver" >                    
						<!-- Thumbnail Image -->
						<img class="preload" src="http://cdn.uwebu.ru/panda/img/6nyQ0_croper_ru.jpeg" alt="image_alt_text" />
					</div> 
					<div class="imageText"> 
						<div class="imageTextHolder">
							<div class="text_field">
								<h5>Компания "UNO ESTATE"</h5>
								<h6>Корпоративный</h6>
								<a class="readMore removeIcon simple detail_btn addBorder roundedRec ajaxPopup_galItem"  href="<?= $u_template_dir ?>/parts/popup-potfolio-content.php" 
									u-mfp-title="UNO ESTATE"
									u-mfp-image="http://cdn.uwebu.ru/panda/img/site/2017-02-08_14-19-32.png"
									u-mfp-desc="Сайт был создан для риелторского агенства. Работа велась, в первую очередь, над главной страницей сайта."
									u-mfp-site="http://sdamkvartiry.moscow"
									>
									Подробнее
									<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
								</a> 
							</div>
						</div>                        
					</div>
				</div>
				<div class="item hover_enable itemOver selPopup" >
					<div class="porImgOver" >                    
						<!-- Thumbnail Image -->
						<img class="preload" src="http://cdn.uwebu.ru/panda/img/At1If_croper_ru.png" alt="image_alt_text" />
					</div> 
					<div class="imageText"> 
						<div class="imageTextHolder">
							<div class="text_field">
								<h5>Компания "Стартерстор"</h5>
								<h6>Интернет-магазин</h6>
								<a class="readMore removeIcon simple detail_btn addBorder roundedRec ajaxPopup_galItem"  href="<?= $u_template_dir ?>/parts/popup-potfolio-content.php" 
									u-mfp-title="Стартерстор"
									u-mfp-image="http://cdn.uwebu.ru/panda/img/site/2017-02-08_15-13-28.png"
									u-mfp-desc="Интернет-магазин по продаже запчастей и комплектующих для грузовых и легковых автомобилей, а также иных типов транспортных средств."
									u-mfp-site="http://starterstore.ru"
									>
									Подробнее
									<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
								</a> 
							</div>
						</div>                        
					</div>
				</div>
				<div class="item hover_enable itemOver selPopup" >
					<div class="porImgOver" >                    
						<!-- Thumbnail Image -->
						<img class="preload" src="http://cdn.uwebu.ru/panda/img/1uQWB_croper_ru.jpeg" alt="image_alt_text" />
					</div> 
					<div class="imageText"> 
						<div class="imageTextHolder">
							<div class="text_field">
								<h5>Строительная компания "Мимиа"</h5>
								<h6>Корпоративный</h6>
								<a class="readMore removeIcon simple detail_btn addBorder roundedRec ajaxPopup_galItem"  href="<?= $u_template_dir ?>/parts/popup-potfolio-content.php" 
									u-mfp-title="Мимиа"
									u-mfp-image="http://cdn.uwebu.ru/panda/img/site/2017-02-08_15-57-12.png"
									u-mfp-desc="Корпоративный сайт СК Мимиа. На &nbsp;данный момент домен для сайта не еще приобретен, так как сайт находится на стадии разработки."
									u-mfp-site="http://str.dmigorevich.ru"
									>
									Подробнее
									<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
								</a> 
							</div>
						</div>                        
					</div>
				</div>
				<div class="item hover_enable itemOver selPopup" >
					<div class="porImgOver" >                    
						<!-- Thumbnail Image -->
						<img class="preload" src="http://cdn.uwebu.ru/panda/img/xcT2d_croper_ru.jpeg" alt="image_alt_text" />
					</div> 
					<div class="imageText"> 
						<div class="imageTextHolder">
							<div class="text_field">
								<h5>Спецтехника "ТТБ"</h5>
								<h6>Интернет-магаз</h6>
								<a class="readMore removeIcon simple detail_btn addBorder roundedRec ajaxPopup_galItem"  href="<?= $u_template_dir ?>/parts/popup-potfolio-content.php" 
									u-mfp-title="Запчасти"
									u-mfp-image="http://cdn.uwebu.ru/panda/img/site/2017-02-08_16-33-02.png"
									u-mfp-desc="Интернет-магазин запчастей для спецтехники. На данный момент сайт находится на тестовом домене, так как процесс разработки еще не окончен."
									u-mfp-site="http://zap.dmigorevich.ru"
									>
									Подробнее
									<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
								</a> 
							</div>
						</div>                        
					</div>
				</div>
				<div class="item hover_enable itemOver selPopup" >
					<div class="porImgOver" >                    
						<!-- Thumbnail Image -->
						<img class="preload" src="http://cdn.uwebu.ru/panda/img/cZ5Ba_croper_ru.jpeg" alt="image_alt_text" />
					</div> 
					<div class="imageText"> 
						<div class="imageTextHolder">
							<div class="text_field">
								<h5>Центральное агенство недвижисмости</h5>
								<h6>Промо-страница</h6>
								<a class="readMore removeIcon simple detail_btn addBorder roundedRec ajaxPopup_galItem"  href="<?= $u_template_dir ?>/parts/popup-potfolio-content.php" 
									u-mfp-title="Центральное агенство недвижимости"
									u-mfp-image="http://cdn.uwebu.ru/panda/img/site/2017-02-08_17-06-59.png"
									u-mfp-desc="Лендинг для агенства недвижимости. Основной целью создания данного сайта является привлечение новых клиентов через контекстную рекламу."
									u-mfp-site="http://can-msk.ru"
									>
									Подробнее
									<span class="icon"><i class="fa fa-arrow-circle-right"></i></span> 
								</a> 
							</div>
						</div>                        


			</div>
		</div>

		<hr class="separator_max">
	</div>
</div>
<!-- About2 us page -->
<div data-id="!about2" class="contentWrapper" >
	<div class="pageContent addPageBorder" >
		<div class="container" >
			<div class="row">
				<div class="col-md-12">
					<div class="page_header text-center" data-animated-in="animated fadeInUp"  data-animated-time="0"  data-animated-innerContent="yes" data-anchor-to="parent">
						<h4 class="mini_heading font_small dark letterSpacing center">
							Наш подход к разработке сайтов
							<i class="fa fa-adn back_icon_title"></i>
						</h4>
						<hr>
					</div>
				</div>
			</div>
			<div class="row"  data-animated-in="animated fadeInLeft"  data-animated-time="0"  data-animated-innerContent="yes" data-anchor-to="parent.parent.parent">
				<div class="col-md-5">
					<dl class="accordion" data-autoHide="true" data-openFirstElement="true"  >

						<dt>
							<a href="index_slider.html" class="normal">
								<span class="closeOpen" ></span>
								<span class="acc_heading bold_weight">
									Прототипирование
									</span>
							</a>
						</dt>
						<dd>
							<div class="acc_content services_list2">
								<!-- Tab content -->
								<span class="icon">
									<i class="icon fa fa-bank fa-3x light_color"></i>
								</span>
								<div class="desc">
									<p>Разработка прототипа сайта необходима для того, чтобы заказчик и разработчик имели чёткое представление об особенностях проекта, находились в одном информационном поле.</p>
								</div>
							</div>
						</dd>

						<dt>
							<a href="index_slider.html" class="normal">
								<span class="closeOpen" ></span>
								<span class="acc_heading bold_weight">
									Пользовательские сценарии
									</span>
							</a>
						</dt>
						<dd>
							<div class="acc_content services_list2">
								<!-- Tab content -->
								<span class="icon">
									<i class="icon fa fa-steam fa-3x light_color"></i>
								</span>
								<div class="desc">
									<p>Целью взаимодействия посетителя с сайтом может быть контакт с компанией, покупка в интернет-магазине, регистрация или другое действие. Мы моделируем сценарии поведения на сайте для различных групп пользователей, планируем навигацию и представление информации таким образом, чтобы сайт приводил пользователей к запланированной цели.</p>
								</div>
							</div>
						</dd>

						<dt>
							<a href="index_slider.html" class="normal">
								<span class="closeOpen" ></span>
								<span class="acc_heading bold_weight">
									Дизайн
									</span>
							</a>
						</dt>
						<dd>
							<div class="acc_content services_list2">
								<!-- Tab content -->
								<span class="icon">
									<i class="icon fa fa-binoculars fa-3x light_color"></i>
								</span>
								<div class="desc">
									<p>Качественный дизайн сайта - это гораздо больше, чем красивая и оригинальная картинка. Он должен разговаривать с целевой аудиторией, акцентировать преимущества ваших предложений, помогать посетителям получить нужную информацию и принять решение о заказе.</p>
								</div>
							</div>
						</dd>

						<dt>
							<a href="index_slider.html" class="normal">
								<span class="closeOpen" ></span>
								<span class="acc_heading bold_weight">
									Разработка
									</span>
							</a>
						</dt>
						<dd>
							<div class="acc_content services_list2">
								<!-- Tab content -->
								<span class="icon">
									<i class="icon fa fa-binoculars fa-3x light_color"></i>
								</span>
								<div class="desc">
									<p>Мы разрабатываем сайт итерационно, с частыми демонстрациями того, что получается. Подобный подход позволяет клиенту четко понимать, на какой стадии выполнения находится сейчас та или иная задача. Важная составляющая раздела «Разработка» – работа с программным кодом, включающая в себя его написание, тестирование, отладку, а также проверку верстки на кросс-браузерность и адаптивность под мобильные устройства.</p>
								</div>
							</div>
						</dd>

						<dt>
							<a href="index_slider.html" class="normal">
								<span class="closeOpen" ></span>
								<span class="acc_heading bold_weight">
									Тех. поддержка
									</span>
							</a>
						</dt>
						<dd>
							<div class="acc_content services_list2">
								<!-- Tab content -->
								<span class="icon">
									<i class="icon fa fa-binoculars fa-3x light_color"></i>
								</span>
								<div class="desc">
									<p>Мы делаем такие сайты, которые позволяют владельцам самостоятельно управлять ими без привлечения специалистов. Мы можем взять на себя все работы по текущей эксплуатации сайта (например, обновление информации) и по его развитию.</p>
								</div>
							</div>
						</dd>
					</dl>​​
				</div>
				<div  class="col-md-7" >
					<img alt="image_alt_text" class="preload scale-with-grid alignCenter" src="<?= $u_assets_path_services ?>/images/about_top_img.png" data-src="<?= $u_assets_path_services ?>/images/about_top_img.png" > 
					<hr>
				</div>
			</div>
			<hr class="separator_mini onlyHigDevice">
		</div>
	</div>
</div>
<!-- About3 us page -->
<div data-id="!about3" class="contentWrapper enablHardwareAcc">
	<div class="pageContent addPageBorder">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="page_header text-center" data-animated-in="animated fadeInUp" data-animated-time="0" data-animated-innercontent="yes" data-anchor-to="parent" style="visibility: visible;">
						<h4 class="mini_heading font_small dark letterSpacing center" style="visibility: visible;">
							ПРЕИМУЩЕСТВА ЗАКАЗА САЙТА У НАС
							<i class="fa fa-adn back_icon_title"></i>
						</h4>
					</div>
				</div>
			</div>
			<div class="row" data-animated-in="animated fadeInLeft" data-animated-time="0" data-animated-innercontent="yes" data-anchor-to="parent" style="visibility: visible;">
				<div class="col-md-3 services_list1" style="visibility: visible;">
					<span class="icon">
					<i class="icon fa fa-bank fa-3x light_color"></i>
					</span>
					<div class="desc">
						<h5>Уникальный, не шаблонный сайт</h5>
						<p>Cоздание сайта мы рассматриваем как творческий процесс, в котором все подчинено определенным бизнес-задачам. Вы получите уникальный ресурс, разработанный в соответствии с вашими требованиями.</p>
					</div>
					<hr>
				</div>
				<div class="col-md-3 services_list1" style="visibility: visible;">
					<span class="icon">
					<i class="icon fa fa-steam fa-3x light_color"></i>
					</span>
					<div class="desc">
						<h5>Первичная SEO-оптимизация</h5>
						<p>Будучи seo-мастерами, мы точно знаем все нюансы и тонкости продвижения, которые обязательно учитываются при разработке сайта. Вам не нужно будет переплачивать за доработки при последующем сео-продвижении.</p>
					</div>
					<hr>
				</div>
				<div class="col-md-3 services_list1" style="visibility: visible;">
					<span class="icon">
					<i class="icon fa fa-binoculars fa-3x light_color"></i>
					</span>
					<div class="desc">
						<h5>Технологичность и широкие возможности</h5>
						<p>Мы располагаем самым современным функционалом, знаем и применяем самые современные маркетинговые, дизайнерские, программные «фишки».</p>
					</div>
					<hr>
				</div>
				<div class="col-md-3 services_list1" style="visibility: visible;">
					<span class="icon">
					<i class="icon fa fa-binoculars fa-3x light_color"></i>
					</span>
					<div class="desc">
						<h5>Гарантированное качество продукта</h5>
						<p>Оптимальный баланс скорости, качества и стоимости разработки сайта. Мы работаем на результат и делаем высокое качество услуг по созданию интернет-ресурса доступным.</p>
					</div>
					<hr>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$services_parts_contact_title = 'Сколько стоит сделать сайт';
$services_parts_contact_content = 'Для понимания точной стоимости и сроков разработки сайта необходим индивидуальный подход к оценке каждого проекта. Стоимость и сроки сильно зависят от задач бизнеса, типа сайта и посадочных страниц, количества подключаемых модулей (галерея, вопрос-ответ, видеовставки, каталог и т.д.). Заполните форму, и наши специалисты свяжутся с вами для обсуждения необходимых деталей.';
require_once 'services-parts/contact.php';
?>
<?php require_once 'services-parts/footer.php'; ?>