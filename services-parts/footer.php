			<!-- contactus page -->
			<div class="fullWidth relativePos" >
				<div class="container-fluid top_bot_pad">
					<div class="row-fluid">
						<div class="col-md-12 text-center" >
							<hr class="separator_mini">
							<h5 class="light_weight tiny_font">2015 - 2017 ООО MEDIAPANDA</h5>
							<h6 class="fontFamily_1">Конфиденциальность</h6>
						</div>
					</div>
				</div>
				<hr class="separator_mini">
			</div>
		</div>
	</div>

	<!-- Included javascript files 
		================================================== -->
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery-1.11.0.min.js"></script> 
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/SmoothScroll.js"></script> 
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.transit.js"></script>    
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.browser.js"></script>    
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/bootstrap.min.js"></script> 
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.touchSwipe.min.js"></script>     
	<!--   	    --> 
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jflickrfeed.min.js"></script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBLJqvcIfq3VDtarh0ykxPgeuHXn-sgC5E"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/supersized.3.2.7.min.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/supersized.shutter.min.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.fitvids.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.nicescroll.min.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquerypp.elastislide.custom.js" ></script>     
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.elastislide.js" ></script>  
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/owl.carousel.min.js" ></script>      
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.cycle.all.js"></script>   
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/portfolio.detail.min.js"></script>  
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.flexslider.min.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jQuery.tubeplayer.js"></script>    
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/isotope-docs.js"></script>    
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.magnific-popup.min.js"></script> 
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/jquery.support.plugin.min.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/custom.min.js"></script> 
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/video.min.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/bigvideo.js"></script> 
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/page_default.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/main-fm.min.js"></script>
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/main.js"></script>
</body>
</html>