			<!-- Contact us page -->
			<div data-id="!contactus" class="contentWrapper" >
				<div class="pageContent addPageBorder" >
					<div class="container" data-animated-time="0" data-animated-in="animated fadeInUp"  data-animated-innerContent="yes" data-anchor-to="parent.parent" >
						<div class="row">
							<div class="col-md-12">
								<div class="page_header noBorder text-center">
									<h4 class="mini_heading font_small dark letterSpacing center">
										<?= $services_parts_contact_title ?>
										<i class="fa fa-coffee back_icon_title"></i>
									</h4>
									<p class="pad_large_xx font_small onlyHigDevice padTop_0 padBot_0" style="font-weight: 100;">
										<span class="pad_large padBot_0">
											<?= $services_parts_contact_content ?>
										</span>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="row-fluid" data-animated-time="1" data-animated-in="animated fadeInUp" data-animated-innerContent="yes"  data-anchor-to="parent.parent.parent" >
							<div class="col-md-5" >
								<div class="tag_leftTitle">
									<span class="icon"><i class="icon-basic-map"></i></span>                    
									<h5 class="light_weight">Россия, г. Москва, ул.</h5>
									<h5 class="light_weight">Щербаковская 3. оф.704</h5>
								</div>
								<div class="tag_leftTitle">
									<span class="icon"><i class="icon-basic-smartphone"></i></span>                    
									<h5 >Телефон</h5>
									<h5 class="light_weight">+7 (926) 649-49-84</h5>
								</div>
								<div class="tag_leftTitle">
									<span class="icon"><i class="icon-basic-paperplane"></i></span> 
									<h5 >Телеграм</h5>
									<h5 class="light_weight">+7 (926) 649-49-84</h5>
								</div>
								<div class="tag_leftTitle">
									<span class="icon"><i class="icon-basic-mail-open"></i></span>                    
									<h5 >Отправить письмо</h5>
									<h5 class="light_weight"><a>info@mediapanda.ru</a></h5>
								</div>
								<br>
								<hr class="separator_mini">
							</div>
							<div class="col-md-7" >
								<h4 class="font_medium_x left_spacing">Напишите нам</h4>
								<hr class="separator_mini">
								<!--Email contact Form-->
								<form name="contact" class="contactusForm" method="post" >
									<div class="row-fluid" >
										<div class="col-md-6">
											<input type="text" value="Имя" id="name" name="name" class="col-md-12 transprentBg dottedBorder" 
												onfocus="if(this.value == 'Имя') {this.value = '';}"	
												onblur="if (this.value == '') {this.value = 'Имя';}" />
										</div>
										<div class="col-md-6">
											<input type="text" value="Email" id="email" name="email" class="col-md-12 transprentBg dottedBorder"  
												onfocus="if(this.value == 'Email') {this.value = '';}"  
												onblur="if (this.value == '') {this.value = 'Email';}" />
										</div>
									</div>
									<div class="row-fluid" >
										<div class="col-md-6">
											<input type="text" value="Ваш сайт" id="phone" name="phone" class="col-md-12 transprentBg dottedBorder" 
												onfocus="if(this.value == 'Ваш сайт') {this.value = '';}"	
												onblur="if (this.value == '') {this.value = 'Ваш сайт';}" />
										</div>
										<div class="col-md-6">
											<input type="text" value="Телефон" id="phone" name="phone" class="col-md-12 transprentBg dottedBorder"  
												onfocus="if(this.value == 'Телефон') {this.value = '';}"  
												onblur="if (this.value == '') {this.value = 'Телефон';}" />
										</div>
									</div>
									<div class="row-fluid">
										<div class="col-md-12">
											<textarea cols="30" rows="5" id="message" name="message" class="col-md-12 transprentBg dottedBorder"	
												onfocus="if(this.value == 'Сообщение...') {this.value = '';}" 
												onblur="if (this.value == '') {this.value = 'Сообщение...';}">Сообщение...</textarea>        
										</div>
									</div>
									<hr>
									<div class="row-fluid">
										<div class="col-md-12 textAlignCenter alignCenter">
											<button type="submit" id="email_submit"  class="button large transprentBg dashedBorder makeAnimate"> 
											Отправить
											</button>
											<div id="reply_message" ></div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="fullWidth map_holder greyBackground relativePos borderTop textAlignCenter">
						<hr class="space_mini">
						 <iframe id="map_canvas" class="pointerCenter removebackground autoLoadOff parallax" src="https://www.google.ru/maps/d/embed?mid=1pwxDv64i0sSaynusIqfWgR_PCXY"></iframe>
						 <style>
						 iframe#map_canvas { -webkit-filter: grayscale(100%); filter: grayscale(100%);}
						 </style>
					</div>
				</div>
			</div>