<!DOCTYPE html>
<html lang="ru">
<head>
	<!-- Basic Page Needs
		================================================== -->
	<meta charset="utf-8">
	<title><?= $title_site ?></title>
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Mobile Specific Metas
		================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!-- CSS
		================================================== --> 
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!-- Favicons
		================================================== -->
	<link rel="shortcut icon" href="<?= $u_assets_path_main ?>/img/favicon.ico">
	<!-- Included CSS files 
		================================================== -->    
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/bootstrap.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/picons.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/linea_icons.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/animate.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/main.min.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/style.min.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/base.min.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/font-style.css" id="set_font" >
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/color-black.css" id="set_color">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/elastislide.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/owl.carousel.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/owl.theme.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/owl.transitions.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/bigvideo.css">
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/magnific-popup.css" media="screen" />
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/flexslider.min.css" media="screen" />
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/supersized.min.css" media="screen" />
	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/supersized.shutter.min.css" media="screen" />

	<link rel="stylesheet" href="<?= $u_assets_path_main ?>/style/frameworks/normalize.css">

	<link rel="stylesheet" href="<?= $u_assets_path_services ?>/css/main.css" />
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript" src="<?= $u_assets_path_services ?>/js/modernizr.min.js"></script>
</head>

<body class="index-isotope menuCloseIt menuAutoClose" data-page="index" data-removePageBorder="yes" data-menuSelected="!home" >
	<!-- PRELOADER START -->
	<div class="preloader">
		<div class="loader">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
	<!-- PRELOADER START -->

	<section id="wrapper" class="wrapper">
		<style> .wrapper { min-height: 570px; } </style>
		<div id="particles-js"></div> 
		<div class="right-side"  style="height:100%;">

			<?php require_once 'nav.php'; ?>

			<!-- FULLPAGE WRAPPER START -->
			<div id="fullpage" class="fullpage-wrapper" style="height: 100%; position: relative; touch-action: none; transform: translate3d(0px, 0px, 0px);">

				<!-- HOME SECTION START -->
				<div class="section home fp-section active fp-table fp-completely" id="!home" data-anchor="home" style="height: 100%;">
					<div class="fp-tableCell" style="height:100%;display: flex;align-items: center;">
						<!-- text section start -->
						<div class="text-section">
							<?= $services_parts_text_section_content ?>
						</div>
						<!-- text section end -->
						<!-- social networks section start -->
						<ul class="social-section animated fadeIn">
							<li><a href="tg://resolve?domain=&lt;ko_ko_konst&gt;"><i class="ion-paper-airplane"></i></a></li>
							<li><a href="info@mediapanda.ru"><i class="ion-email"></i></a></li>
						</ul>
						<!-- social networks section end -->
					</div>
				</div>
				<!-- HOME SECTION END -->
			</div>
			<!-- FULLPAGE WRAPPER START -->
		</div>
		<div class="alignCenter flickerAnimate fadeInBig add-visible-ready" data-animated-time="50" data-animated-in="fadeInBig" data-anchor-to="parent.parent" style="visibility: visible; position: absolute; bottom: 3vh; text-align: right; padding-right: 4vw; width: 100%;">
			<a class="link_btn relativePos menu_link u-js-scroll-to-data" to="!about">
				<span class="icon textAlignCenter"><i class="fa fa-arrow-circle-o-down  light_color fa-3x"></i></span>
			</a>
		</div>
	</section>


	<!-- Complete web site content is placed inside the bodyContainer class div except footer -->
	<div class="bodyContainer">
		<div class="mainContent" >