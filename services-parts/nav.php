<!-- navigation start -->
<nav class="animated fadeIn">
	<button class="menu-btn">
		<svg version="1.1" id="menuicn" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 54 33" enable-background="new 0 0 54.238 33" xml:space="preserve">
			<rect y="30" class="first" width="54.238" height="3"></rect>
			<rect x="0" y="20" class="inbetween" width="44.238" height="3"></rect>
			<rect x="0" y="10" class="inbetween" width="34.238" height="3"></rect>
			<rect width="54.238" class="last" height="3"></rect>
		</svg>
	</button>
	<ul class="">
		<li data-menuanchor="home"><a href="<?= $u_site_url ?>/#home">главная</a></li>
		<li data-menuanchor="about"><a href="<?= $u_site_url ?>/#about">о компании</a></li>
		<li data-menuanchor="skills"><a href="<?= $u_site_url ?>/#skills">навыки</a></li>
		<li data-menuanchor="services" class="active"><a href="<?= $u_site_url ?>/#services">услуги</a>
			<ul class="sub-nav show">
				<li class="<?= ($id_service == 0) ? 'active' : '' ?>"><a href="<?= $u_site_url ?>/services-seo/">SEO</a></li>
				<li class="<?= ($id_service == 1) ? 'active' : '' ?>"><a href="<?= $u_site_url ?>/services-pr/">Ваш рекламщик</a></li>
				<li class="<?= ($id_service == 2) ? 'active' : '' ?>"><a href="<?= $u_site_url ?>/services-context/">Контекст</a></li>
				<li class="<?= ($id_service == 3) ? 'active' : '' ?>"><a href="<?= $u_site_url ?>/services-smm/">SMM</a></li>
				<li class="<?= ($id_service == 4) ? 'active' : '' ?>"><a href="<?= $u_site_url ?>/services-seo/">Наружка</a></li>
				<li class="<?= ($id_service == 5) ? 'active' : '' ?>"><a href="<?= $u_site_url ?>/services-digital/">Digital</a></li>
				<li class="<?= ($id_service == 6) ? 'active' : '' ?>"><a href="<?= $u_site_url ?>/services-site/">Сайты</a></li>
			</ul>
		</li>
		<li data-menuanchor="portfolio"><a href="<?= $u_site_url ?>/#portfolio">портфолио</a></li>
		<li data-menuanchor="news"><a href="<?= $u_site_url ?>/#news">новости</a></li>
		<li data-menuanchor="contacts"><a href="<?= $u_site_url ?>/#contacts">контакты</a></li>
	</ul>
</nav>
<!-- navigation end -->