<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package panda-theme
 */

?>
<?php $u_assets_path_blog = get_template_directory_uri() . '/assets/blog'; ?>
<?php $u_assets_path_main = get_template_directory_uri() . '/assets/main'; ?>

			</div>
	
			<?php get_template_part( 'template-parts/content-sidebar'  ); ?>

		</div>

	</div>
	<div class="panda_instagram container-fluid">

		<!-- SERVICES SECTION START -->
		<div class="services">
			<!-- SERVICES CAROUSEL WRAPPER START -->
			<div class="slick-carousel">

				<!-- services item start -->
				<li><a href="#" target="_blank">
					<div class="item" data-tilt>
							<div class="hexagon">
								<div class="left"></div>
								<div class="center"></div>
								<div class="right"></div>
							</div>
							<div class="content">
								<h1>SEO</h1>
							</div>
					</div>
				</a></li>
				<!-- services item end -->

				<!-- services item start -->
				<li><a href="#" target="_blank">
					<div class="item" data-tilt>
						<div class="hexagon">
							<div class="left"></div>
							<div class="center"></div>
							<div class="right"></div>
						</div>
						<div class="content">
							<h1>PR</h1>
						</div>
					</div>
				</a></li>
				<!-- services item end -->

				<!-- services item start -->
				<li><a href="#" target="_blank">
					<div class="item" data-tilt>
						<div class="hexagon">
							<div class="left"></div>
							<div class="center"></div>
							<div class="right"></div>
						</div>
						<div class="content">
							<h1>Контекст</h1>
						</div>
					</div>
				</a></li>
				<!-- services item end -->

				<!-- services item start -->
				<li><a href="#" target="_blank">
					<div class="item" data-tilt>
						<div class="hexagon">
							<div class="left"></div>
							<div class="center"></div>
							<div class="right"></div>
						</div>
						<div class="content">
							<h1>SMM</h1>
						</div>
					</div>
				</a></li>
				<!-- services item end -->

				<!-- services item start -->
				<li><a href="#" target="_blank">
					<div class="item" data-tilt>
						<div class="hexagon">
							<div class="left"></div>
							<div class="center"></div>
							<div class="right"></div>
						</div>
						<div class="content">
							<h1>Наружная реклама</h1>
						</div>
					</div>
				</a></li>
				<!-- services item end -->

				<!-- services item start -->
				<li><a href="#" target="_blank">
					<div class="item" data-tilt>
						<div class="hexagon">
							<div class="left"></div>
							<div class="center"></div>
							<div class="right"></div>
						</div>
						<div class="content">
							<h1>Digital</h1>
						</div>
					</div>
				</a></li>
				<!-- services item end -->

				<!-- services item start -->
				<li><a href="#" target="_blank">
					<div class="item" data-tilt>
						<div class="hexagon">
							<div class="left"></div>
							<div class="center"></div>
							<div class="right"></div>
						</div>
						<div class="content">
							<h1>Разработка сайтов</h1>
						</div>
					</div>
				</a></li>
				<!-- services item end -->

				<!-- services item start -->
				<li><a href="#" target="_blank">
					<div class="item" data-tilt>
						<div class="hexagon">
							<div class="left"></div>
							<div class="center"></div>
							<div class="right"></div>
						</div>
						<div class="content">
							<h1>Разработка ПО</h1>
						</div>
					</div>
				</a></li>
				<!-- services item end -->

			</div>
			<!-- SERVICES CAROUSEL WRAPPER END -->

		</div>
		<!-- SERVICES SECTION END -->
		
		<a class="panda_up">
			<h6>Наверх</h6>
			<div class="panda_up_line"></div>
		</a>			
	</div>
	<footer class="container-fluid">

		<p class="u-mt">Политика конфиденциальности</p>

		<!-- <ul class="panda_social"> -->
<!-- 			<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li> -->
		<!-- </ul> -->
		<ul class="panda_footer_contact">
			<li><small>+7 (926) 649-49-84 <a href="tg://resolve?domain=<ko_ko_konst>"><i style="color:#2097D4; font-size: 20px;" class="fa fa-telegram"></i></a></small></li>
			<li><small>2015 - 2017 ООО MEDIAPANDA.</small></li>
			<li><small>info@mediapanda.ru</small></li>
		</ul>
	</footer>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tilt.js/1.1.21/tilt.jquery.min.js"></script>
	<script src="<?= $u_assets_path_main ?>/js/plugins.js"></script>
	<script src="<?= $u_assets_path_blog ?>/js/libs.min.js"></script>
	<script src="<?= $u_assets_path_blog ?>/js/common.js"></script>

<?php wp_footer(); ?>

</body>
</html>
