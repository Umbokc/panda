<div class="white_ver ajaxPopupPage ajax-text-and-image white-popup-block">
        
    
    <!-- Complete web site content is placed inside the bodyContainer class div except footer -->
                    
    <div class="container">
    
        <hr class="separator_mini" />
        
        <div class="row">
            <h4 class="mini_heading font_small dark letterSpacing center u-the-title-popup">
                Империя света
                <i class="fa fa-adn back_icon_title"></i>
            </h4>
        </div>
        
         <div class="row">
         
            <div class="col-md-12">                  
                <div class=" relativePos" >
                    <hr>                
                    <div class="flexSlideshow  slideAnimation showControlNav posDefault" >
                        <ul class="slides"> 
                            <li><img class="u-the-image-popup" src="" alt="Some text"/></li>
                        </ul>
                    </div> 
                </div>
                
            </div>  
                    
         
        </div> 
        
        <hr class="separator_mini"> 
        <hr>
        
        <div class="row">  
            <div class="col-md-8">
                <p class="u-the-desc-popup">Создание интернет-магазина по продаже светильников, люстр, ламп и расходных материалов</p>
            </div>
                                   
            <div class="col-md-4">                  
                <ul class="item_feature">
                    <li><span class="title"><i class="fa fa-external-link"></i></span><a class="u-the-site-popup" href="http://imperiya-sveta.moscow" target="_blank">http://imperiya-sveta.moscow</a></li>
                </ul>
            </div>
        </div> 
        <hr>         
            
    </div>

    
    <!-- Included javascript files 
    ================================================== -->  
    <!-- <script type="text/javascript" src="js/portfolio_popup.js"></script>  -->
    <script>
    "use strict";
    $(document).ready(function(){
        // Flex slideshow initialize
        $("body").find('.flexSlideshow').each(function(){
            try{
                
                var aniTyp = $(this).hasClass('slideAnimation') ? "slide" : "fade";
                var tim_ = $(this).attr('data-slidetime') ?  Math.abs($(this).attr('data-slidetime')) : 5000;
                
                
                if(aniTyp === "slide"){
                    $(this).find("li").each(function(i){
                        $(this).find("img").show();
                    });
                }

                $(this).addClass("flexslider");     
                var ffx = $(this);
                ffx.removeClass('flexSlideshow');

                var flexs = $(this);
                
                flexs.flexslider({
                    slideshow: true,
                    animation: aniTyp,
                    slideshowSpeed: tim_,
                    start: function(slider){
                        flexs.data("slid",slider);
                        flexs.find(".slider_loading").remove();
                        slider.pause();
                    }
                }); 
                
            } catch (e) { }             
        });
        
        try {  $(".container").fitVids(); } catch (e) { }
        try {  $(".container-fluid").fitVids(); } catch (e) { } 

        var top = $('[data-id="!portfolio"]').offset().top;
        $('.mfp-bg, .mfp-wrap').css({'top': top+'px'});
    });     
    </script>
</div>