<?php

$errorMSG = "";

// NAME INPUT
if (empty($_POST["name"])) {
    $errorMSG = "Name is required ";
} else {
    $name = $_POST["name"];
}

// EMAIL INPUT
if (empty($_POST["email"])) {
    $errorMSG .= "Email is required ";
} else {
    $email = $_POST["email"];
}

// MESSAGE INPUT
if (empty($_POST["message"])) {
    $errorMSG .= "Message is required ";
} else {
    $message = $_POST["message"];
}

//INPUT FOR YOUR EMAIL ADDRESS
// $EmailTo = "maintest@gmail.com";
$EmailTo = "dr-stepan@mail.ru";

$Subject = "New Message Received";

// LETTER FORMATTING
$Body = "";
$Body .= "Name: ";
$Body .= $name;
$Body .= "\n";
$Body .= "Email: ";
$Body .= $email;
$Body .= "\n";
$Body .= "Message: ";
$Body .= $message;
$Body .= "\n";

// SENDING EMAIL
$success = mail($EmailTo, $Subject, $Body, "From:".$email);

// TO SUCCESS PAGE
if ($success && $errorMSG == ""){
   echo "success";
}else{
    if($errorMSG == ""){
        echo "Something went wrong :(";
    } else {
        echo $errorMSG;
    }
}

?>
