<?php

if ( post_password_required() ) {
	return;
}
?>

<article class="panda_comments">
	<h5>Комментарии к этому посту</h5>
	<?php
	
	if ( have_comments() ) : ?>

		<?php // the_comments_navigation(); ?>

		<?php
			wp_list_comments( array(
				'style' => 'div',
				'max_depth' => 2,
				'type' => 'comment',
				'short_ping' => true,
				'callback' => function($comment, $args, $depth){
					echo '<div>';
					?>
					<div class="panda_comment <?= ($depth != 1) ? 'panda_comment2' : '' ?>">
						<div >
							<?php echo get_avatar( $comment, $size='48', $default='' ); ?>
						</div>
						<div>
							<p class="panda_text"><?php comment_text() ?></p>
							<div class="panda_comment_author"><?php echo get_comment_author_link() ?></div>
							<time> <?php printf( '%1$s в %2$s', get_comment_date(),  get_comment_time()) ?></time>
							<a class="panda_reference" ><?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?></a>
						</div>
					</div>
				<?php
				},
				'end-callback' => function($comment, $args, $depth){
					echo '</div>';
				}
			) );
		?>

		<?php


	else: 
		?>
	<p>Комментариев пока нет.</p>
		<?php
	endif;
	?>

</article><!-- #comments -->
