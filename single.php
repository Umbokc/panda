<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package panda-theme
 */

get_header(); ?>


		<?php
			the_post();

			get_template_part( 'template-parts/content-page', get_post_type() );

		?>

<?php
get_sidebar();
get_footer();
