<?php $u_template_dir = get_template_directory_uri(); ?>
<?php $u_site_url = get_site_url(); ?>
<?php $u_assets_path_main = $u_template_dir . '/assets/main'; ?>
<?php $u_assets_path_services = $u_template_dir . '/assets/services'; ?>

<?php
$id_service = 0;
$title_site = 'SEO продвижение';
$services_parts_text_section_content = '
<div class="pre-title animated fadeInDown">
	Мы креативное агенство
</div>
<!-- text rotator start -->
<div class="title morphext animated fadeIn">
	SEO продвижение
</div>
<!-- text rotator end -->
<p class="descrip">
	Цель — увеличение продаж и продвижение бренда
	<ul class="list">
		<li>Индивидуальный дизайн</li>
		<li>Удобная система управления контентом</li>
		<li>Хостинг (размещение) сайта</li>
		<li>Техническая поддержка</li>
	</ul>
</p>
';
require_once 'services-parts/header.php'; ?>

<!-- About us page -->
<div data-id="!about" class="contentWrapper">
	<div class="pageContent addPageBorder u-js-style-h-auto!" >
		<div class="container" data-animated-time="0" data-animated-in="animated fadeInUp" data-animated-innerContent="yes" data-anchor-to="parent" >
			<div class="row"> 	
				<div class="col-md-12">      	
					<div class="page_header text-center"> 

						<h4 class="mini_heading font_small dark letterSpacing center">
							SEO продвижение
							<i class="fa fa-qq back_icon_title"></i>
						</h4>

						<div class=" textAlignCenter" >
							<h5 class="light_weight font_small">
								Скорее всего, Вы слышали о SEO продвижении сайтов. Но даже если Вам никогда не приходилось сталкиваться с этим понятием, то, безусловно, Вы пользуетесь поисковыми системами (Яндекс, Google, Yahoo и т.д.), суть которых заключается в выдаче списка ответов на определенный запрос пользователя (например, «купить автомобиль», «ремонт квартир в Москве», «заказать продвижение сайта»). Обычно на любой запрос, поисковик выдает список из сотен тысяч сайтов. Какие-то веб-ресурсы оказываются на первых местах поисковой выдачи, чтобы найти другие, придется пролистать ни одну страницу. Порядок расположения сайтов по конкретному запросу определятся сложными алгоритмами поисковых систем.  Чем большему количеству критериев соответствует сайт, тем выше он будет находиться в выдаче. Краткое определение понятия поисковая оптимизация (SEO) можно сформулировать как комплекс мер, направленных на повышение позиций сайта в органической выдаче поисковых систем по определенным запросом.
							</h5>
						</div>

						<hr>
					</div> 
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row" data-animated-time="0" data-animated-in="animated fadeInUp" data-animated-innercontent="yes" data-anchor-to="parent" >               
				<div class="col-md-7" >
					<!-- Flex Slider with frame image -->    
					<div class="frame_slideshow">    
						<div class="flexSlideshow hideDirectionNav" >
							<ul class="slides"> 
								<li><a  class="lazyload" href="<?= $u_assets_path_services ?>/images/seo/424543546457645.jpg" title="Some text">image</a></li>
								<li><a  class="lazyload" href="<?= $u_assets_path_services ?>/images/seo/seo-search-engine-optimization-ss.jpg" title="Some text">image</a></li>
							</ul>
							<hr>
							<hr class="slide_bottomSpace">
						</div> 
					</div>
				</div>
				<div class="col-md-5">
					<div class="pad_large onlyHigDevice" >
						<h4 class="font_medium_x">Аудитория</h4>
						<p class="fontFamily_1">На сегодняшний день всемирная паутина является мощным рекламным инструментом и местом распространения товаров и услуг в режиме online. Количество людей, использующих интернет исчисляется миллиардами. По данным ООН пользователей сети интернет больше чем людей, имеющих постоянный и беспрепятственный доступ к питьевой воде. В нашей стране ежедневно пользуются интернетом около 100 миллионов человек. Таким образом, используя интернет, можно привлечь гораздо более обширную аудиторию, чем имея физическую точку распространения товаров и услуг.</p>
						<hr class="separator_mini">	
					</div>   
				</div>
			</div>
		</div>
		<div class="container" data-animated-time="0" data-animated-in="animated fadeInUp" data-animated-innerContent="yes" data-anchor-to="parent" >
			<div class="row"> 	
				<div class="col-md-12">      	
					<div class="text-center"> 

						<h4 class="mini_heading font_small dark letterSpacing center">
							Что вы получите от SEO продвижения?
							<i class="fa fa-lightbulb-o back_icon_title"></i>
						</h4>

						<div class=" textAlignCenter" >
							<h5 class="light_weight font_small">
								Продвижение в интернете поможет Вам увеличить свою прибыль в независимости от сферы Вашей деятельности. Сейчас в интернете ищут все. Люди предпочитают покупать товары, сидя дома за компьютером. Это и не удивительно. Зачем куда-то ехать, тратить много времени, если можно то же самое купить просто нажав кнопку на смартфоне? При чем со временем, эта тенденция будет приобретать все более массовый характер. Задумайтесь, сотни тысяч людей каждый день занимаются поиском Ваших товаров в интернете, но вместо Вас, они находят Ваших конкурентов. Закажите поисковую оптимизацию у нас, и Ваш сайт окажется в ТОП выдачи поисковых систем!
							</h5>
						</div>
					</div> 
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="page_header text-center" data-animated-in="animated fadeInUp" data-animated-time="0" data-animated-innercontent="yes" data-anchor-to="parent" style="visibility: visible;">
						<h4 class="mini_heading font_small dark letterSpacing center" style="visibility: visible;">
							Почему необходимо, чтобы сайт был как можно ближе к первой строчке органической выдачи?
							<i class="fa fa-search back_icon_title"></i>
						</h4>
					</div>
				</div>
			</div>
			<div class="row" data-animated-in="animated fadeInLeft" data-animated-time="0" data-animated-innercontent="yes" data-anchor-to="parent.parent" style="visibility: visible;">
				<div class="col-md-4 services_list2" style="visibility: visible;">
					<span class="icon">
					<i class="icon fa fa-bank fa-3x light_color"></i>
					</span>
					<div class="desc">
						<h5>Статистика</h5>
						<p>Статистика показывает, что менее 10% пользователей откроют вторую страницу поисковой выдачи, только 2% перейдут на третью страницу, если Ваш сайт находится еще ниже, то, скорее всего, Вас не увидит никто. Почему же так происходит? Ответ на этот вопрос кроется в психологии человека. Люди склонны искать простые пути во всем. Зачем выходить из дома, если можно купить в интернете? Зачем копаться в выдаче поисковика, если все и так можно найти на первой странице?</p>
					</div>
					<hr>
				</div>
				<div class="col-md-4 services_list2" style="visibility: visible;">
					<span class="icon">
					<i class="icon fa fa-steam fa-3x light_color"></i>
					</span>
					<div class="desc">
						<h5>ТОП выдачи</h5>
						<p>Необходимо заметить, что чем выше сайт располагается в органической выдаче, тем больше ему доверяет пользователь. Люди привыкли, что самые качественные, удобные и полезные сайты находятся на первой странице поисковой выдачи. А если сайт занимает первую строчку, то и продукция, представленная на нем, должна быть лучше, чем у конкурентов, бессознательно решает потенциальный клиент. Такое стереотипное мышление людей позволяет сайтам, которые находятся в ТОП выдачи, забирать львиную долю трафика и клиентов.</p>
					</div>
					<hr>
				</div>
				<div class="col-md-4 services_list2" style="visibility: visible;">
					<span class="icon">
					<i class="icon fa fa-binoculars fa-3x light_color"></i>
					</span>
					<div class="desc">
						<h5>Рекомендации</h5>
						<p>Рекомендация нашей фирмы проста. Если хотите получать прибыль при помощи сайта, то Ваш веб-ресурс должен находиться в ТОП-5 выдачи поисковиков, хотя бы по наиболее важным запросам.</p>
					</div>
					<hr>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row" data-animated-time="0" data-animated-in="animated fadeInUp" data-animated-innercontent="yes" data-anchor-to="parent.parent" >               
				<div class="col-md-6" >
					<figure class="large_image">
						<img class="preload alignCenter scale-with-grid" src="<?= $u_assets_path_services ?>/images/service_imac_img2.png" alt="image text">
					</figure>
				</div>
				<div class="col-md-6">
					<div class="pad_large onlyHigDevice" >
						<h4 class="font_medium_x">По каким фразам лучше всего раскручивать сайт в интернете?</h4>
						<p class="fontFamily_1">Поисковое продвижение позволит Вам в разы увеличить входящий трафик. Но посетитель сайта далеко не всегда становится покупателем. Для повышения процента конверсии (соотношение посетителей к покупателям) важно выбрать правильные фразы для продвижения. Ваш сайт должен давать максимально релевантный ответ на запрос пользователя. В противном случае, даже при повышении трафика, число клиентов может остаться неизменным. Поэтому, перед началом продвижения, очень важно подобрать корректные ключевые фразы.</p>
						<hr class="separator_mini">	
					</div>   
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$services_parts_contact_title = 'Заказать продвижение в социальных сетях';
$services_parts_contact_content = 'Стоимость данного вида услуг зависит от многих факторов. Поэтому мы рекомендуем Вам проконсультироваться с менеджером нашей компании, который максимально доступно на «языке людей» ответит на все Ваши вопросы и даст рекомендации по бюджету.';
require_once 'services-parts/contact.php';
?>
<?php require_once 'services-parts/footer.php'; ?>