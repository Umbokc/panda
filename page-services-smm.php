<?php $u_template_dir = get_template_directory_uri(); ?>
<?php $u_site_url = get_site_url(); ?>
<?php $u_assets_path_main = $u_template_dir . '/assets/main'; ?>
<?php $u_assets_path_services = $u_template_dir . '/assets/services'; ?>

<?php
$id_service = 3;
$title_site = 'Агентство интернет-маркетинга Media Panda профессионально занимается SMM (Social Media Marketing).';
$services_parts_text_section_content = '
<div class="pre-title animated fadeInDown">
	Мы креативное агенство
</div>
<!-- text rotator start -->
<div class="title morphext animated fadeIn">
	Разработка сайтов
</div>
<!-- text rotator end -->
<p class="descrip">
	Цель — увеличение продаж и продвижение бренда
	<ul class="list">
		<li>Индивидуальный дизайн</li>
		<li>Удобная система управления контентом</li>
		<li>Хостинг (размещение) сайта</li>
		<li>Техническая поддержка</li>
	</ul>
</p>';

require_once 'services-parts/header.php'; ?>

<!-- About us page -->
<div data-id="!about" class="contentWrapper">
	<div class="pageContent addPageBorder u-js-style-h-auto!" >
		<div class="owlSlider owl-carousel owl-theme owlSingleSlider navSmall" data-touchDrag="no" >

			<div class="owlDirNav navShowAlways">
				<div class="owlNextBtn"><span class="icon"><i class="fa fa-angle-right fa-2x"></i></span></div>
				<div class="owlPrevBtn"><span class="icon"><i class="fa fa-angle-left fa-2x"></i></span></div>
			</div>

			<div class="portion relativePos greyBackground" >
				<div class="container-fluid pad_0" >
					<div class="row-fluid fullHeight fitHeight" > 
						<div class="col-md-6 pad_0"> 
							<div class="fullHeight blackBackground  makeFit mobFitRemove parallax" data-src="http://cdn.uwebu.ru/panda/img/smm/01_Professioanalism.jpg" >
								<hr class="separator_TopBot">                                                                
							</div>
						</div>
						<div class="col-md-6 pad_0 fullHeight  makeFit mobFitRemove"> 
							<div class="pad_large_xx onlyHigDevice">
								<div class="relativePos" >
									<h1 class="upperCase onlyHigDevice padBot_0" >Профессионализм</h1>
									<i class="icon icon-basic-paperplane fa-4x back_icon bottom"></i>
								</div>
								<hr class="separator_mini onlyHigDevice"> 
								<hr class="separator_bar dark">  
								<hr class="space_mini onlyHigDevice"> 
								<p class="mini_description onlyHigDevice">Агентство интернет-маркетинга Media Panda профессионально занимается SMM (Social Media Marketing) и продвижением в социальных сетях Facebook, Instagram, ВКонтакте. Многие компании недооценивают эффективность использования социальных сетей в коммерческих целях. По посещаемости с социальными сетями, на сегодняшний день, могут сравниться лишь поисковые системы. Только Facebook посещают более 22 миллиардов раз в месяц. Это огромная аудитория с самыми разными интересами. </p>  
								<hr class="separator_max">
							</div>  
						</div>
					</div>
				</div>
			</div>

			<div class="portion relativePos greyBackground" >
				<div class="container-fluid pad_0" >
					<div class="row-fluid fullHeight fitHeight" > 
						<div class="col-md-6 pad_0"> 
							<div class="fullHeight blackBackground  makeFit mobFitRemove parallax" data-src="http://cdn.uwebu.ru/panda/img/smm/02_Prodvigenie.jpg" >
								<hr class="separator_TopBot">                                                                
							</div>
						</div>
						<div class="col-md-6 pad_0 fullHeight  makeFit mobFitRemove"> 
							<div class="pad_large_xx onlyHigDevice">
								<div class="relativePos" >
									<h1 class="upperCase onlyHigDevice padBot_0" >Продвижение</h1>
									<i class="icon icon-basic-paperplane fa-4x back_icon bottom"></i>
								</div>
								<hr class="separator_mini onlyHigDevice"> 
								<hr class="separator_bar dark">  
								<hr class="space_mini onlyHigDevice"> 
								<p class="mini_description onlyHigDevice">Продвижение в социальных сетях позволит Вам расширить охват аудитории, а, соответственно, увеличит поток клиентов для Вашего бизнеса. Для некоторых направлений деятельности, продвижение в социальных сетях, является самым быстрым и дешевым способом привлечения новых клиентов. К таковым можно отнести: продажа одежды и аксессуаров, салоны красоты, распространение различных косметических средств, продажа обуви и т.д. В этих сферах деятельности социальные сети могут иметь большую отдачу, чем полноценный интернет-магазин.</p>  
								<hr class="separator_max">
							</div>  
						</div>
					</div>
				</div>
			</div>

			<div class="portion relativePos greyBackground" >
				<div class="container-fluid pad_0" >
					<div class="row-fluid fullHeight fitHeight" > 
						<div class="col-md-6 pad_0"> 
							<div class="fullHeight blackBackground  makeFit mobFitRemove parallax" data-src="http://cdn.uwebu.ru/panda/img/smm/03_Aktualnost.jpg" >
								<hr class="separator_TopBot">                                                                
							</div>
						</div>
						<div class="col-md-6 pad_0 fullHeight  makeFit mobFitRemove"> 
							<div class="pad_large_xx onlyHigDevice">
								<div class="relativePos" >
									<h1 class="upperCase onlyHigDevice padBot_0" >Актуальность</h1>
									<i class="icon icon-basic-paperplane fa-4x back_icon bottom"></i>
								</div>
								<hr class="separator_mini onlyHigDevice"> 
								<hr class="separator_bar dark">  
								<hr class="space_mini onlyHigDevice"> 
								<p class="mini_description onlyHigDevice">Наша компания понимает актуальность продвижения в социальных сетях, поэтому у нас работают профессионалы своего дела, с многолетним опытом работы. Мы также являемся сертифицированным партнером ВКонтакте. Обратившись к нам, вы получите результат, от которого сами будете в шоке. Мы предлагаем лучшие цены на рынке интернет-маркетинга, а главное, результат нашей работы не заставит себя долго ждать. Уже в первый месяц вы сможете пожинать плоды деятельности специалистов компании Media Panda.</p>  
								<hr class="separator_max">
							</div>  
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- About us page -->

<?php
$services_parts_contact_title = 'Заказать продвижение в социальных сетях';
$services_parts_contact_content = 'На сегодняшний день, необходимость продвижения в социальных сетях очевидна. Любой современный бизнесмен понимает, что большая часть населения нашей страны, так или иначе, являются пользователями соц. сетей. Более того, поисковые системы считают отсутствие сайта в социальных сетях негативным фактором ранжирования, что может не лучшим образом сказаться на SEO-продвижении. Оправьте нам заявку или позвоните. Наши специалисты проконсультируют Вас по всем интересующим вопросам.';
require_once 'services-parts/contact.php';
?>
<?php require_once 'services-parts/footer.php'; ?>