<?php 
return array(
	'u_pto_sevices' => array(
		'label' => 'Услуги',
		'type' => 'sub-section',
		'sections' => array(
			'site' => array(
				'label' => 'Разработка сайтов',
				'type' => 'sub-section',
				'sections' => array(
					'first_section' => array(
						'label' => 'Первый экран',
						'fields' => array(
							'under-title' => array(
								'label' => 'Надпись над заголовок',
								'type' => 'input',
								'default' => 'Мы креативное агенство'
							),
							'title' => array(
								'label' => 'Заголовок',
								'type' => 'input',
								'default' => 'Разработка сайтов'
							),
							'descrip' => array(
								'label' => 'Описание',
								'type' => 'wp_editor',
								'cols' => '6',
								'default' => "Цель — увеличение продаж и продвижение бренда\n<ul class=\"list\">\n\t<li>Индивидуальный дизайн</li>\n\t<li>Удобная система управления контентом</li>\n\t<li>Хостинг (размещение) сайта</li>\n\t<li>Техническая поддержка</li>\n</ul>"
							),
						)
					),
					'second_section' => array(
						'label' => 'Какие сайты мы делаем?',
						'fields' => array(
							'title' => array(
								'label' => 'Заголовок',
								'type' => 'input',
								'default' => 'Какие сайты мы делаем?'
							),
							'h3_1' => array(
								'label' => 'Блоки',
								'type' => 'h3',
								'default' => 'Разработка сайтов'
							),
							'block-1' => array(
								'label' => 'Первый блок',
								'type' => 'tab',
								'fields' => array(
									'icon' => array(
										'label' => 'Иконка',
										'type' => 'input',
										'default' => 'icon-basic-lightbulb'
									),
									'title' => array(
										'label' => 'Заголовок',
										'type' => 'input',
										'default' => 'Промо-страница'
									),
									'sub-title' => array(
										'label' => 'Под заголовок',
										'type' => 'input',
										'default' => 'от 25 000 руб'
									),
									'text' => array(
										'label' => 'Текст',
										'type' => 'wp_editor',
										'cols' => '3',
										'default' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.'
									),
								),
							),
							'block-2' => array(
								'label' => 'Второй блок',
								'type' => 'tab',
								'fields' => array(
									'icon' => array(
										'label' => 'Иконка',
										'type' => 'input',
										'default' => 'icon-basic-heart'
									),
									'title' => array(
										'label' => 'Заголовок',
										'type' => 'input',
										'default' => 'Корпоративный сайт'
									),
									'sub-title' => array(
										'label' => 'Под заголовок',
										'type' => 'input',
										'default' => 'от 80 000 руб'
									),
									'text' => array(
										'label' => 'Текст',
										'type' => 'wp_editor',
										'cols' => '3',
										'default' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.'
									),
								),
							),
							'block-3' => array(
								'label' => 'Третий блок',
								'type' => 'tab',
								'fields' => array(
									'icon' => array(
										'label' => 'Иконка',
										'type' => 'input',
										'default' => 'icon-basic-paperplane'
									),
									'title' => array(
										'label' => 'Заголовок',
										'type' => 'input',
										'default' => 'Информационный портал'
									),
									'sub-title' => array(
										'label' => 'Под заголовок',
										'type' => 'input',
										'default' => 'от 40 000 руб'
									),
									'text' => array(
										'label' => 'Текст',
										'type' => 'wp_editor',
										'cols' => '3',
										'default' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.'
									),
								),
							),
							'block-4' => array(
								'label' => 'Четвертый блок',
								'type' => 'tab',
								'fields' => array(
									'icon' => array(
										'label' => 'Иконка',
										'type' => 'input',
										'default' => 'icon-basic-cup'
									),
									'title' => array(
										'label' => 'Заголовок',
										'type' => 'input',
										'default' => 'Интернет магазин'
									),
									'sub-title' => array(
										'label' => 'Под заголовок',
										'type' => 'input',
										'default' => 'от 120 000 руб'
									),
									'text' => array(
										'label' => 'Текст',
										'type' => 'wp_editor',
										'cols' => '3',
										'default' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.'
									),
								),
							),
						)
					),
					// 'third_section' => array(
						
					// ),
				),
			),
		),
	),
);