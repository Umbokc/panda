<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package panda-theme
 */

get_header(); ?>

	<div id="panda_post9" class="container panda_blog_listing">
		<div class="row panda_main">
			<div class="col-md-9 panda_content ">

			<?php 
				while ( have_posts() ) : the_post();
				echo "string";
				the_title();
					get_template_part( 'template-parts/content', get_post_format() );
				endwhile;
			 ?>

				<section>

					<div class="panda_post_block">
						<ul>
							<li>Feb 28, 2017</li>
							<li>by Pixel-Mafia</li>
							<li>in Branding   +   Design</li>
						</ul>
						<h2>Input vs. Output: How to Set Smarter Goals</h2>
						<img src="img/panda_posts_img2.jpg" alt="">
						<p class="panda_text">Proin quis arcu ex. Nullam eu dolor magna. Sed consequat tincidunt purus eget ullamcorper. Quisque pellentesque pellentesque scelerisque. Maecenas egestas sagittis erat et accumsan. Pellentesque pretium pretium ex, et porta magna mattis in. Etiam tempus lacus quis nisi lobortis, a volutpat augue accumsan. Vivamus ut aliquam est.</p>
						<a class="panda_reference" href="post.html">read more</a>
					</div>

				</section>
				<section>

					<div class="panda_post_block">
						<ul>
							<li>Feb 28, 2017</li>
							<li>by Pixel-Mafia</li>
							<li>in Branding   +   Design</li>
						</ul>
						<h2>Increase Productivity, Keep Your Soul</h2>
						<img src="img/panda_posts_img3.jpg" alt="">
						<p class="panda_text">Proin quis arcu ex. Nullam eu dolor magna. Sed consequat tincidunt purus eget ullamcorper. Quisque pellentesque pellentesque scelerisque. Maecenas egestas sagittis erat et accumsan. Pellentesque pretium pretium ex, et porta magna mattis in. Etiam tempus lacus quis nisi lobortis, a volutpat augue accumsan. Vivamus ut aliquam est.</p>

						<a class="panda_reference" href="post.html">read more</a>
					</div>

				</section>
				<div class="panda_pagination">
					<ul>
						<li><a href="#"><i class="fa fa-arrow-left" aria-hidden="true"></i></a><a href="#">prev</a></li>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">...</a></li>
						<li><a href="#">6</a></li>
						<li><a href="#">next</a><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>

			<aside class="col-md-3 panda_sidebar">
				<?php // get_sidebar(); ?>
				<article class="panda_about_sidebar">
					<h6>about</h6>
					<p class="panda_text">Exploring the crowd &amp; processes creating understandable products for digital and analogic folks.</p>
					<p class="panda_text">Pixies conceptualizes, designs &amp; delivers the happiness.</p>
				</article>
				<article class="panda_search">

					<form>
						<label for="search"></label>
						<input type="search" name="search" id="search" placeholder="search">
						<button name="submit"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
					</form>
				</article>
				<article class="panda_categories">
					<h6>Categories</h6>
					<ul>
						<li><a href="#">Strategy</a><span>(3)</span></li>
						<li><a href="#">Technology</a><span>(5)</span></li>
						<li><a href="#">Creative</a><span>(2)</span></li>
						<li><a href="#">Content</a><span>(8)</span></li>
					</ul>
				</article>
				<article class="panda_posts">
					<h6>Features Posts</h6>
					<ul>
						<li>
							<img src="img/sidebar_post_img1.jpg" alt="">
							<p >57 Ways to Become a Photographer</p>
							<time datetime="2017-02-28">Feb 28, 2017</time>
							<div class="clear"></div>
						</li>
						<li>
							<img src="img/sidebar_post_img2.jpg" alt="">
							<p>Our Latest Project Nominated for Awwwards</p>
							<time datetime="2017-02-26">Feb 26, 2017</time>
							<div class="clear"></div>
						</li>
					</ul>
				</article>
				<article class="panda_tags">
					<h6>Popular Tags</h6>
					<ul>
						<li><a href="#">art</a></li>
						<li><a href="#">design</a></li>
						<li><a href="#">concept</a></li>
						<li><a href="#">Digital</a></li>
						<li><a href="#">Media</a></li>
						<li><a href="#">Photography</a></li>
						<li><a href="#">product</a></li>
					</ul>
				</article>
				<article class="panda_banner">
					<img src="img/banner_img.jpg" alt="">
				</article>
			</aside>
		</div>

	</div>
	<div class="panda_instagram container-fluid">

		<h5>instagram</h5>
		<h2>#panda</h2>
		<ul>
			<li>
				<a href="#">
					<img src="img/instagram_img1.jpg" alt="">
				</a>
			</li>
			<li>
				<a href="#">
					<img src="img/instagram_img2.jpg" alt="">
				</a>
			</li>
			<li>
				<a href="#">
					<img src="img/instagram_img3.jpg" alt="">
				</a>
			</li>
			<li>
				<a href="#">
					<img src="img/instagram_img4.jpg" alt="">
				</a>
			</li>
			<li>
				<a href="#">
					<img src="img/instagram_img5.jpg" alt="">
				</a>
			</li>
			<li>
				<a href="#">
					<img src="img/instagram_img6.jpg" alt="">
				</a>
			</li>
		</ul>
		<a class="panda_up">
			<h6>Top</h6>
			<div class="panda_up_line"></div>
		</a>			
	</div>

<?php

get_footer();
