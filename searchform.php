
<article class="panda_search">
	<form  role="search" method="get" id="searchform" action="<?php echo home_url( '/blog' ) ?>">
		<label for="search"></label>
		<input type="search" value="<?php echo get_search_query() ?>" name="s" id="s" placeholder="Поиск">
		<button name="searchsubmit" type="submit" id="searchsubmit"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
	</form>
</article>