<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package panda-theme
 */

get_header(); ?>
<?php $u_assets_path_blog = get_template_directory_uri() . '/assets/blog'; ?>

			<?php if ( have_posts() ) : ?>

				<?php 
					while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/content', get_post_format() );
					endwhile;

				?>
				<?php $args = array(
					'prev_next'          => false,
					'type'               => 'array',
				); ?>
				<div class="panda_pagination">
					<ul>
						<li><?php previous_posts_link( '<i class="fa fa-arrow-left" aria-hidden="true"></i> Предыдущая страница' ); ?></li>
						<?php 
							
							$nav = paginate_links($args);
							$nav = count($nav);
							for ($i=1; $i < $nav+1; $i++) {
									echo "<li>
										<a href=\"?paged=$i\">
											$i
										</a>
									</li>";
							}

						 ?>
						<li><?php next_posts_link( 'Следующая страница <i class="fa fa-arrow-right" aria-hidden="true"></i>' ); ?></li>
					</ul>
				</div>
			<?php else : ?>
				<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
			<?php endif; ?>
<?php

get_footer();
