/*
Template Name: TRIME. - Creative Comin Soon OnePage HTML Template
Author: GuroThemes
Author URI: https://themeforest.net/user/gurothemes
Version: 1.0
*/

/*
TABLE OF CONTENTS:

1.PRELOADER
2.INITIALIZATION
3.FUNCTION TEXT ROTATOR
4.FUNCTION TIME COUNTER
5.FUNCTION NOTIFY
6.FUNCTION CAROUSEL
7.FUNCTION SKILLBAR
8.FUNCTION PROGRESS COUNTERS
9.FUNCTION FULLPAGE
10.FUNCTIONS PARTICLE VARIABLES
	1)Default
	2)Nasa
	3)Bubble
	4)Snow
11.FUNCTION LIGHTBOX
12.FUNCTION ADD ONWHEEL LISTENER
*/

/* --------------------------------
	PRELOADEER
-------------------------------- */
$(window).on("load", function() {
	setTimeout(function() {
		 $('.preloader').addClass('is-load');
	}, 1000);
});


/* --------------------------------
	INITIALIZATION
-------------------------------- */
$(document).ready(function () {

	"use strict"

	page();
	carousel();
	lightbox();
	textRotator();
	practicleBubble();

});


/* --------------------------------
	FUNCTION TEXT ROTATOR
-------------------------------- */
function textRotator(params) {
	$(".name-title").Morphext({
		// ANIMATION TYPE. YOU CAN GET ANIMATION NAMES FROM ANIMATION.CSS
		animation: "flipInX",
		// TEXT SEPARATOR
		separator: ",",
		// THE DELAY BETWEEN CHANGING WORDS
		speed: 2000
	});
};


/* --------------------------------
	FUNCTION TIME COUNTER
-------------------------------- */
function timeCounter(params) {
	setTimeout(function () {
		$(".counter-section").addClass('animated fadeIn');
	}, 2000);

	$(".counter-section").counter({
		endTime: new Date("Jul 25, 2019 12:59:00 UTC+0200")
	});

	$(".counter-section-mobile").counter({
		endTime: new Date("Jul 25, 2019 12:59:00 UTC+0200")
	});
};


/* --------------------------------
	FUNCTION NOTIFY
-------------------------------- */
function notify(params) {
	$("#notifyMe").notifyMe();
};


/* --------------------------------
	FUNCTION CAROUSEL
-------------------------------- */
function carousel(params) {
	var owl = $('.owl-carousel');
	owl.owlCarousel({
		loop:false,
		margin:10,
		stagePadding: 50,
		center: true,
		startPosition: 1,
		nav:true,
		 navText : ["<i class='ion-ios-arrow-left'></i>","<i class='ion-ios-arrow-right'></i>"],
		rewindNav : true,
		// onDragged: function(e){
		// },
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1280:{
				items:2
			},
			1400:{
				items:3
			}
		}
	});

	var slick_carousel = $('.slick-carousel');
	slick_carousel.slick({
		nextArrow: '<div class="slick-next owl-next animated fadeIn"><i class="ion-ios-arrow-right"></i></div>',
		prevArrow: '<div class="slick-prev owl-prev animated fadeIn"><i class="ion-ios-arrow-left"></i></div>',
		centerMode: true,
		// centerPadding: '60px',
		slidesToShow: 1,
		infinite:false,
		variableWidth:true,
		initialSlide:0,
		dots:true,
		responsive: [
			{
				breakpoint: 480,
				settings: {
					initialSlide:0
				}
			}
    ]
	});

};


/* --------------------------------
	FUNCTION SKILLBAR
-------------------------------- */
function skillBar(params) {
	$('.skill-item').each(function() {
		$(this).find('.skill-bar').animate({
			width: $(this).attr('data-skill')
		}, 4000);
	});
};


/* --------------------------------
	FUNCTION PROGRESS COUNTERS
-------------------------------- */
function counters(params) {
	$('.counter-item span').each(function() {
		var $this = $(this),
			countTo = $this.attr('data-count');

		$({countNum: $this.text()}).animate({
			countNum: countTo
		}, {

			duration: 8000,
			easing: 'linear',
			step: function() {
				$this.text(Math.floor(this.countNum));
			},
			complete: function() {
				$this.text(this.countNum);

			}
		});
	});
};


/* --------------------------------
	FUNCTION FULLPAGE
-------------------------------- */
function page(params) {
	/* navigation initialization */
	setTimeout(function () {
		$('nav').addClass('animated fadeIn');
	}, 2300);

	/* navigation btn initialization */
	$('.menu-btn').on("click", function() {
		$(this).toggleClass('menu-btn-open');
		$('.wrapper').toggleClass('toggle-wrapp');
		setTimeout(function() {
			$('nav ul').toggleClass('toggle-nav');
		}, 200);
		
	});

	/* fullpage load */
	setTimeout(function () {

		var fDown = $('.bg-title, .pre-title'),
			fUp = $('.descrip, .list, .owl-carousel, .skill-block, .counters-block '),
			fIn = $('.title, .name-title, .social-section, .contact-inf, .contact-form, .news-form, .counter-section-mobile'),
			carouselCtr = $('.owl-next, .owl-prev');

		$('#fullpage').fullpage({
			anchors: ['home', 'about', 'skills', 'services', 'portfolio', 'news', 'contacts'],
			menu: 'nav',
			scrollOverflow:true,
			controlArrows: false,
			scrollingSpeed: 700,
			afterLoad: function (anchorLink, index) {
				var loadedSection = $(this);

				/* transform page after scrolling */
				if (index == 2 || 3 || 4) {
					$('.wrapper').addClass('is-scroll');
				}
				console.log(anchorLink);

				/* initialization skill and counters by viewport */
				if (anchorLink == 'skills') {
					$('.sub-nav').css({'display':'none', 'visibility':'hidden'});
					skillBar();
					counters();
				}else if(anchorLink == 'services'){

					$('.sub-nav').css({'display':'block', 'visibility':'visible'});

					// On before slide change
					$('.slick-carousel').on('beforeChange', function(event, slick, currentSlide, nextSlide){
						$('.sub-nav li').removeClass('active');
						$('.sub-nav li').eq(nextSlide).addClass('active');
						// console.log(nextSlide);
					});

					$('.sub-nav li').removeClass('active');
					$('.sub-nav li').eq($('.slick-carousel').slick('slickCurrentSlide')).addClass('active');
					console.log($('.slick-carousel').slick('slickCurrentSlide'));
				}else{
					$('.sub-nav').css({'display':'none', 'visibility':'hidden'});
					$('.skill-bar').css('width', '0');
					$('.counter-item span').each(function() {
						var $this = $(this);

						$({countNum: $this.text()}).animate({
							countNum: '0'
						}, {

							duration: 1,
							step: function() {
								$this.text(Math.floor(this.countNum));
							},
							complete: function() {
								$this.text(this.countNum);

							}
						});
					});
				}

				if (index == 1) {
					$('.wrapper').removeClass('is-scroll');
				}

				if (loadedSection) {
					var l = $(this);
					l.find(fDown).addClass('animated fadeInDown');
					l.find(fUp).addClass('animated fadeInUp');
					l.find(fIn).addClass('animated fadeIn');
					setTimeout(function () {
						l.find(carouselCtr).addClass('animated fadeIn');
					}, 1000);

				};
			},

			onLeave: function (index, nextIndex, direction) {
				var leavingSection = $(this);

				if (leavingSection) {
					var l = $(this);
					l.find(fDown).addClass('fadeOutUp');
					l.find(fUp).addClass('fadeOutDown');
					l.find(fIn).addClass(' fadeOut');
					l.find(carouselCtr).addClass(' fadeOut');


					setTimeout(function () {
						l.find(fDown).removeClass('animated fadeInDown fadeOutUp');
						l.find(fUp).removeClass('animated fadeInUp fadeOutDown');
						l.find(fIn).removeClass('animated fadeIn fadeOut');
						l.find(carouselCtr).removeClass('animated fadeIn fadeOut');
					}, 1000);
				};
			}
		});
	}, 2600);
};


/* --------------------------------
	FUNCTIONS PARTICLE VARIABLES
-------------------------------- */
function practicleDefault(params) {

	particlesJS("particles-js", {
		"particles": {
			"number": {
				"value": 25,
				"density": {
					"enable": true,
					"value_area": 800
				}
			},
			"color": {
				"value": "#ffffff"
			},
			"shape": {
				"type": "circle",
				"stroke": {
					"width": 0,
					"color": "#000000"
				},
				"polygon": {
					"nb_sides": 5
				},
				"image": {
					"src": "img/github.svg",
					"width": 100,
					"height": 100
				}
			},
			"opacity": {
				"value": 0.5,
				"random": false,
				"anim": {
					"enable": false,
					"speed": 1,
					"opacity_min": 0.1,
					"sync": false
				}
			},
			"size": {
				"value": 3,
				"random": true,
				"anim": {
					"enable": false,
					"speed": 40,
					"size_min": 0.1,
					"sync": false
				}
			},
			"line_linked": {
				"enable": true,
				"distance": 150,
				"color": "#ffffff",
				"opacity": 0.4,
				"width": 1
			},
			"move": {
				"enable": true,
				"speed": 6,
				"direction": "none",
				"random": false,
				"straight": false,
				"out_mode": "out",
				"bounce": false,
				"attract": {
					"enable": false,
					"rotateX": 600,
					"rotateY": 1200
				}
			}
		},
		"interactivity": {
			"detect_on": "canvas",
			"events": {
				"onhover": {
					"enable": false,
					"mode": "repulse"
				},
				"onclick": {
					"enable": false,
					"mode": "push"
				},
				"resize": true
			},
			"modes": {
				"grab": {
					"distance": 400,
					"line_linked": {
						"opacity": 1
					}
				},
				"bubble": {
					"distance": 400,
					"size": 40,
					"duration": 2,
					"opacity": 8,
					"speed": 3
				},
				"repulse": {
					"distance": 200,
					"duration": 0.4
				},
				"push": {
					"particles_nb": 4
				},
				"remove": {
					"particles_nb": 2
				}
			}
		},
		"retina_detect": true
	});
};


function practicleNasa(params) {

	particlesJS("particles-js", {
		"particles": {
			"number": {
				"value": 100,
				"density": {
					"enable": true,
					"value_area": 800
				}
			},
			"color": {
				"value": "#ffffff"
			},
			"shape": {
				"type": "circle",
				"stroke": {
					"width": 0,
					"color": "#000000"
				},
				"polygon": {
					"nb_sides": 5
				},
				"image": {
					"src": "img/github.svg",
					"width": 100,
					"height": 100
				}
			},
			"opacity": {
				"value": 1,
				"random": true,
				"anim": {
					"enable": true,
					"speed": 1,
					"opacity_min": 0,
					"sync": false
				}
			},
			"size": {
				"value": 3,
				"random": true,
				"anim": {
					"enable": false,
					"speed": 4,
					"size_min": 0.3,
					"sync": false
				}
			},
			"line_linked": {
				"enable": false,
				"distance": 150,
				"color": "#ffffff",
				"opacity": 0.4,
				"width": 1
			},
			"move": {
				"enable": true,
				"speed": 1,
				"direction": "none",
				"random": true,
				"straight": false,
				"out_mode": "out",
				"bounce": false,
				"attract": {
					"enable": false,
					"rotateX": 600,
					"rotateY": 600
				}
			}
		},
		"interactivity": {
			"detect_on": "canvas",
			"events": {
				"onhover": {
					"enable": false,
					"mode": "bubble"
				},
				"onclick": {
					"enable": false,
					"mode": "repulse"
				},
				"resize": true
			},
			"modes": {
				"grab": {
					"distance": 400,
					"line_linked": {
						"opacity": 1
					}
				},
				"bubble": {
					"distance": 250,
					"size": 0,
					"duration": 2,
					"opacity": 0,
					"speed": 3
				},
				"repulse": {
					"distance": 400,
					"duration": 0.4
				},
				"push": {
					"particles_nb": 4
				},
				"remove": {
					"particles_nb": 2
				}
			}
		},
		"retina_detect": true
	});
};


function practicleBubble(params) {

	particlesJS("particles-js", {
		"particles": {
			"number": {
				"value": 6,
				"density": {
					"enable": true,
					"value_area": 800
				}
			},
			"color": {
				"value": "#1b1e34"
			},
			"shape": {
				"type": "polygon",
				"stroke": {
					"width": 0,
					"color": "#000"
				},
				"polygon": {
					"nb_sides": 6
				},
				"image": {
					"src": "img/github.svg",
					"width": 100,
					"height": 100
				}
			},
			"opacity": {
				"value": 0.3,
				"random": true,
				"anim": {
					"enable": false,
					"speed": 1,
					"opacity_min": 0.1,
					"sync": false
				}
			},
			"size": {
				"value": 160,
				"random": false,
				"anim": {
					"enable": true,
					"speed": 10,
					"size_min": 40,
					"sync": false
				}
			},
			"line_linked": {
				"enable": false,
				"distance": 200,
				"color": "#ffffff",
				"opacity": 1,
				"width": 2
			},
			"move": {
				"enable": true,
				"speed": 8,
				"direction": "none",
				"random": false,
				"straight": false,
				"out_mode": "out",
				"bounce": false,
				"attract": {
					"enable": false,
					"rotateX": 600,
					"rotateY": 1200
				}
			}
		},
		"interactivity": {
			"detect_on": "canvas",
			"events": {
				"onhover": {
					"enable": false,
					"mode": "grab"
				},
				"onclick": {
					"enable": false,
					"mode": "push"
				},
				"resize": true
			},
			"modes": {
				"grab": {
					"distance": 400,
					"line_linked": {
						"opacity": 1
					}
				},
				"bubble": {
					"distance": 400,
					"size": 40,
					"duration": 2,
					"opacity": 8,
					"speed": 3
				},
				"repulse": {
					"distance": 200,
					"duration": 0.4
				},
				"push": {
					"particles_nb": 4
				},
				"remove": {
					"particles_nb": 2
				}
			}
		},
		"retina_detect": true
	});
};

function practicleSnow(params) {

	particlesJS("particles-js", {
		"particles": {
			"number": {
				"value": 50,
				"density": {
					"enable": true,
					"value_area": 800
				}
			},
			"color": {
				"value": "#fff"
			},
			"shape": {
				"type": "circle",
				"stroke": {
					"width": 0,
					"color": "#000000"
				},
				"polygon": {
					"nb_sides": 5
				},
				"image": {
					"src": "img/github.svg",
					"width": 100,
					"height": 100
				}
			},
			"opacity": {
				"value": 0.5,
				"random": true,
				"anim": {
					"enable": false,
					"speed": 1,
					"opacity_min": 0.1,
					"sync": false
				}
			},
			"size": {
				"value": 10,
				"random": true,
				"anim": {
					"enable": false,
					"speed": 40,
					"size_min": 0.1,
					"sync": false
				}
			},
			"line_linked": {
				"enable": false,
				"distance": 500,
				"color": "#ffffff",
				"opacity": 0.4,
				"width": 2
			},
			"move": {
				"enable": true,
				"speed": 6,
				"direction": "bottom",
				"random": false,
				"straight": false,
				"out_mode": "out",
				"bounce": false,
				"attract": {
					"enable": false,
					"rotateX": 600,
					"rotateY": 1200
				}
			}
		},
		"interactivity": {
			"detect_on": "canvas",
			"events": {
				"onhover": {
					"enable": false,
					"mode": "bubble"
				},
				"onclick": {
					"enable": false,
					"mode": "repulse"
				},
				"resize": true
			},
			"modes": {
				"grab": {
					"distance": 400,
					"line_linked": {
						"opacity": 0.5
					}
				},
				"bubble": {
					"distance": 400,
					"size": 4,
					"duration": 0.3,
					"opacity": 1,
					"speed": 3
				},
				"repulse": {
					"distance": 200,
					"duration": 0.4
				},
				"push": {
					"particles_nb": 4
				},
				"remove": {
					"particles_nb": 2
				}
			}
		},
		"retina_detect": true
	});
};

/* --------------------------------
	FUNCTION LIGHTBOX
-------------------------------- */
var u_pswp_gallery;
// build items array
var u_pswp_gallery_items = [];

function lightbox(params) {
	var pswpElement = document.querySelectorAll('.pswp')[0];

	$('.u-pswp-elements').find('.item').each(function(index, el) {
		u_pswp_gallery_items.push({
			src: $(el).find('img').attr('src'),
			w: 1200,
			h: 900
		});
		$(el).click(function(event) {
			openPhotoSwipe({
				index: index,
				loop: false,
				closeOnScroll:false,
			});
			change_scroll(false);
		});
	});

	var openPhotoSwipe = function(options) {
		// Initializes and opens PhotoSwipe
		u_pswp_gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, u_pswp_gallery_items, options);
		u_pswp_gallery.init();

		// Gallery starts closing
		u_pswp_gallery.listen('close', function() {
			change_scroll(true);
		});
	}

	add_onwheel_listener([
		[document.getElementsByClassName('pswp')[0], lightbox_scroll],
	]);
};

var can_i_move = true;
function lightbox_scroll(e){
	e = e || window.event;

	var delta = e.deltaY || e.detail || e.wheelDelta;
	// console.log(delta);

	if( can_i_move && ((delta < 0 && u_pswp_gallery.getCurrentIndex() == 0) 
		|| (delta > 0 && u_pswp_gallery.getCurrentIndex() == u_pswp_gallery.options.getNumItemsFn()-1))){
		u_pswp_gallery.close();
	}

	if(can_i_move){
		can_i_move = false;

		if(delta > 0)
			u_pswp_gallery.next();
		else
			u_pswp_gallery.prev();

		setTimeout(function(){
			can_i_move = true;
		}, 1000);
	}

}

function change_scroll(params){
	$.fn.fullpage.setMouseWheelScrolling(params);
	$.fn.fullpage.setAllowScrolling(params);
}

/* --------------------------------
	FUNCTION ADD ONWHEEL LISTENER
-------------------------------- */
function add_onwheel_listener(params){
	// example params
	// var params = [
	// 	[elem, onScrollOnFunction],
	// ];

	for (var i = 0; i < params.length; i++) {
		var the_elem = params[i][0];
		var the_func = params[i][1];

		if (the_elem.addEventListener) {
			if ('onwheel' in document) {
				// IE9+, FF17+
				the_elem.addEventListener("wheel", the_func);
			} else if ('onmousewheel' in document) {
				// устаревший вариант события
				the_elem.addEventListener("mousewheel", the_func);
			} else {
				// Firefox < 17
				the_elem.addEventListener("MozMousePixelScroll", the_func);
			}
		} else { // IE8-
			the_elem.attachEvent("onmousewheel", the_func);
		}
	}
}