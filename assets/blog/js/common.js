"use strict";

jQuery(document).ready(function(){

    var panda_js_bg_image = jQuery('.panda_js_bg_image');
    if (jQuery(panda_js_bg_image).length > 0) {
        jQuery(panda_js_bg_image).each(function () {
            jQuery(this).css('background-image', 'url(' + jQuery(this).attr('data-src') + ')');
        });
    }

    // Up Button

    $('.panda_up').on('click', function() {
        $('html, body').animate({scrollTop: 0},200);
        return false;
    })

    // Mobile Menu

        if (jQuery('.mobile_header').length > 0) {
        jQuery('.mobile_header').after('<div class="mobile_menu_wrapper"><ul class="mobile_menu container"/></div>');
        jQuery('.mobile_menu').html(jQuery('.panda_menu_cont').find('ul.panda_menu').html());
        jQuery('.mobile_menu_wrapper').hide();
        jQuery('.btn_mobile_menu').on('click', function () {
            jQuery('.mobile_menu_wrapper').stop().slideToggle(300);
            jQuery('.panda_header').toggleClass('opened');
        });
    }

    // Sharing

    var window_width = jQuery(window).width(),
        window_height = jQuery(window).height(),
        button = jQuery('.panda_sharing_url_button'),
        input = jQuery('.panda_sharing_url');


    jQuery('.panda_sharing_button').on('click', function(){
        var window_width = jQuery(window).width(),
        window_height = jQuery(window).height(),
        panda_sharing_popup = jQuery('.panda_sharing_popup');


    setTimeout("window_height = jQuery(window).height();", 100);

    panda_sharing_popup.width(window_width).height(window_height);
        jQuery(panda_sharing_popup).addClass('active');
    });

    jQuery('.panda_sharing_popup_close').on('click', function(){
        jQuery('.panda_sharing_popup').removeClass('active');
    });

    button.on("click", function(event) {
        event.preventDefault();
        input.select();
    document.execCommand("copy");
        jQuery('.panda_title_default').addClass('inactive');
        setTimeout("jQuery('.panda_title_default').css('display', 'none')", 300);
        setTimeout("jQuery('.panda_title_copied').css('display', 'block');", 300);
        setTimeout("jQuery('.panda_title_copied').addClass('active');", 350);
    });

    // About Carousel

    $('.owl-carousel1').owlCarousel({
        items: 1,
        loop: true,
        slideSpeed: 200,
        nav: true,
        navText: ["",""],
        dots: true,
        autoplay: true,
        autoplayTimeout: 2000,
        smartSpeed: 200,
        autoplayHoverPause: true,
    });

    // Works Isotope Grid

    var $grid = $('.grid').imagesLoaded().progress( function() {
        $grid.isotope({
            itemSelector: '.grid-item',
            layoutMode: 'fitRows'
        });
    }); 

    // bind filter button click
    $('.filters-button-group').on( 'click', 'button', function() {
        var filterValue = $( this ).attr('data-filter');
        // use filterFn if matches value
      
    $grid.isotope({ filter: filterValue });
    });
    // change is-checked class on buttons
    $('.button-group').each( function( i, buttonGroup ) {
        var $buttonGroup = $( buttonGroup );
        $buttonGroup.on( 'click', 'button', function() {
        $buttonGroup.find('.is-checked').removeClass('is-checked');
        $( this ).addClass('is-checked');
        });
    });

    // Works Isotope Packery

    var $grid1 = $('.grid1').imagesLoaded().progress( function() {
        $grid1.isotope({
            layoutMode: 'packery',  
            itemSelector: '.grid-item'
      
        });
    });
  
      // bind filter button click
    $('.filters-button-group').on( 'click', 'button', function() {
        var filterValue = $( this ).attr('data-filter');
        // use filterFn if matches value
  
        $grid1.isotope({ filter: filterValue });
    });
        // change is-checked class on buttons
    $('.button-group').each( function( i, buttonGroup ) {
        var $buttonGroup = $( buttonGroup );
            $buttonGroup.on( 'click', 'button', function() {
            $buttonGroup.find('.is-checked').removeClass('is-checked');
            $( this ).addClass('is-checked');
        });
    });

    // About Studio Counters

    $('#counts').viewportChecker({
        callbackFunction: function(elem, action){
            $(".spincrement").spincrement({
                thousandSeparator: "",
                duration: 2000
            });
        },
    });

    // About Resume Progressbar

    $('.panda_resume_capabilities').viewportChecker({
        callbackFunction: function(elem, action){
        $('.progressbar').each(function(){
      var t = $(this),
        dataperc = t.attr('data-perc'),
        barperc = Math.round(dataperc*5.69);
      t.find('.bar').animate({width:barperc}, dataperc*4);
      t.find('.label').append('<div class="perc"></div>');
      
          function perc() {
            var length = t.find('.bar').css('width'),
              perc = Math.round(parseInt(length, 10)/5.69),
              labelpos = (parseInt(length, 10)-25);
            t.find('.label').css('left', labelpos);
            t.find('.perc').text(perc+'%');
          }
            perc();
            setInterval(perc, 0); 
        });
        },
    });


    // Works Ribbon

    $('.panda_ribbon_slider').slick({
        centerMode: true,
        speed: 200,
        autoplay: true,
        centerPadding: '27.3%',
        responsive: [
        {
            breakpoint: 991,
            settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px'       
            }
        },  
        ]
    });

    var slick_carousel = $('.slick-carousel');
    slick_carousel.slick({
        nextArrow: '<div class="slick-next owl-next animated fadeIn"><i class="ion-ios-arrow-right"></i></div>',
        prevArrow: '<div class="slick-prev owl-prev animated fadeIn"><i class="ion-ios-arrow-left"></i></div>',
        // centerMode: true,
        // centerPadding: '60px',
        slidesToShow: 4,
        infinite:false,
        // variableWidth:true,
        initialSlide:0,
        dots:false,
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    initialSlide:0
                }
            }
    ]
    });

});

jQuery(window).on('load', function(){
    var window_width = jQuery(window).width(),
        window_height = jQuery(window).height();

    if (jQuery('div').is('.panda_portfolio_listing_cont.view_type_full')) {
        var container = jQuery('.panda_portfolio_listing_cont.view_type_full');

        jQuery(container).find('.panda_thumbnails_container').width(window_width).height(window_height);
        jQuery(container).find('.panda_portfolio_content_container').width(window_width).height(window_height);
        panda_portfolio_listing_view_type_fullscreen();
    }

    var window_width = jQuery(window).width(),
        window_height = jQuery(window).height();

    if (jQuery('div').is('.panda_portfolio_listing_cont.view_type_full_2')) {
        var panda_port_cont = jQuery('.panda_portfolio_listing_cont.view_type_full_2'),
            panda_all_item = jQuery(panda_port_cont).attr('data-all-item'),
            panda_trigger_height = window_height / panda_all_item;

        jQuery('body').height(window_height);
        jQuery(panda_port_cont).width(window_width).height(window_height);
        jQuery('.panda_triggers_cont a').height(panda_trigger_height);
        panda_portfolio_listing_view_type_fullscreen_type_2();
    }

    // Carousel

    $('.owl-carousel2').owlCarousel({
        margin:0,
        loop:true,
        slideSpeed:200,
        nav: true,
        navText: ["",""],
        dots: true,
        autoplay: true,
        autoplayTimeout:2000,
        smartSpeed:200,
        autoplayHoverPause:true,
        autoWidth:true,
        items:4
    });
});

jQuery(window).resize(function(){
    var window_width = jQuery(window).width(),
        window_height = jQuery(window).height();

    if (jQuery('div').is('.panda_portfolio_listing_cont.view_type_full')) {
        var container = jQuery('.panda_portfolio_listing_cont.view_type_full');

        jQuery(container).find('.panda_thumbnails_container').width(window_width).height(window_height);
        jQuery(container).find('.panda_portfolio_content_container').width(window_width).height(window_height);
    }

    var window_width = jQuery(window).width(),
        window_height = jQuery(window).height();

    if (jQuery('div').is('.panda_portfolio_listing_cont.view_type_full_2')) {
        var panda_port_cont = jQuery('.panda_portfolio_listing_cont.view_type_full_2'),
            panda_all_item = jQuery(panda_port_cont).attr('data-all-item'),
            panda_trigger_height = window_height / panda_all_item;

        jQuery('body').height(window_height);
        jQuery(panda_port_cont).width(window_width).height(window_height);
        jQuery('.panda_triggers_cont a').height(panda_trigger_height);
    }
});
    
    // Fullscreen 

function panda_portfolio_listing_view_type_fullscreen() {
    jQuery('.panda_portfolio_listing_cont.view_type_full').each(function(){
        var container = jQuery(this),
            image_container = container.find('.panda_thumbnails_container'),
            content_container = container.find('.panda_portfolio_content_container'),
            content_output_wrapper = content_container.find('.panda_portfolio_content_wrapper'),
            items_count = image_container.children().length,
            thumb_item = image_container.find('.panda_image_cont'),
            content_item = content_output_wrapper.find('.panda_portfolio_cont');

        setTimeout("jQuery('.panda_portfolio_listing_cont.view_type_full').css('opacity', '1')", 500);

        if (thumb_item.is('.current_item')) {} else {
            thumb_item.first().addClass('current_item');
            content_item.first().addClass('current_item');
        }

        // Next Post
        jQuery('.panda_next_nav_button').on('click', function(){
            panda_next_post();
        });

        jQuery('body').on('wheel', function(){
            panda_next_post();
        });

        // Previous Post
        jQuery('.panda_prev_nav_button').on('click', function(){
            panda_prev_post();
        });

        function panda_next_post() {
            var current_thumb = image_container.find('.current_item'),
                thumb_number = current_thumb.attr('data-number'),
                counter = container.attr('data-counter');

            if (counter == '1') {
                counter++;
                jQuery(container).attr('data-counter', counter);
                image_container.addClass('action');
                jQuery(image_container).find('.prev_item').removeClass('prev_item');

                if (thumb_number < items_count) {
                    jQuery(image_container).find('.current_item').next().addClass('next_item');
                    setTimeout("jQuery('.panda_portfolio_content_wrapper').find('.current_item').removeClass('current_item').next().addClass('current_item')", 600);
                } else {
                    thumb_item.first().addClass('next_item');
                    setTimeout("jQuery('.panda_portfolio_content_wrapper').find('.current_item').removeClass('current_item')", 600);
                    setTimeout("jQuery('.panda_portfolio_content_wrapper .panda_portfolio_cont').first().addClass('current_item')", 600);
                }

                setTimeout("jQuery('.panda_thumbnails_container').find('.current_item').removeClass('current_item').addClass('prev_item')", 600);
                setTimeout("jQuery('.panda_thumbnails_container').find('.next_item').removeClass('next_item').addClass('current_item')", 600);
                setTimeout("jQuery('.panda_thumbnails_container').removeClass('action')", 1200);
                setTimeout("jQuery('.panda_portfolio_listing_cont.view_type_full').attr('data-counter', '1')", 1800);
            }
        }

        function panda_prev_post() {
            var current_thumb = image_container.find('.current_item'),
                thumb_number = current_thumb.attr('data-number'),
                counter = container.attr('data-counter');

            if (counter == '1') {
                counter++;
                jQuery(container).attr('data-counter', counter);
                image_container.addClass('action');
                jQuery(image_container).find('.prev_item').removeClass('prev_item');

                if (thumb_number == '1') {
                    thumb_item.last().addClass('next_item');
                    setTimeout("jQuery('.panda_portfolio_content_wrapper').find('.current_item').removeClass('current_item')", 600);
                    setTimeout("jQuery('.panda_portfolio_content_wrapper .panda_portfolio_cont').last().addClass('current_item')", 600);
                } else {
                    jQuery(image_container).find('.current_item').prev().addClass('next_item');
                    setTimeout("jQuery('.panda_portfolio_content_wrapper').find('.current_item').removeClass('current_item').prev().addClass('current_item')", 600);
                }

                setTimeout("jQuery('.panda_thumbnails_container').find('.current_item').removeClass('current_item').addClass('prev_item')", 600);
                setTimeout("jQuery('.panda_thumbnails_container').find('.next_item').removeClass('next_item').addClass('current_item')", 600);
                setTimeout("jQuery('.panda_thumbnails_container').removeClass('action')", 1200);
                setTimeout("jQuery('.panda_portfolio_listing_cont.view_type_full').attr('data-counter', '1')", 1800);
            }
        }
    });
}
    
    // Portfolio

function panda_portfolio_listing_view_type_fullscreen_type_2() {
    jQuery('.panda_portfolio_listing_cont.view_type_full_2').each(function(){
        var container = jQuery(this),
            image_container = container.find('.panda_thumbnails_container'),
            content_container = container.find('.panda_portfolio_content_container'),
            triggers_container = container.find('.panda_triggers_cont'),
            items_count = image_container.children().length,
            thumb_item = image_container.find('.panda_image_cont'),
            trigger_item = triggers_container.find('.panda_portfolio_trigger'),
            content_item = content_container.find('.panda_portfolio_cont_item');

        setTimeout("jQuery('.panda_portfolio_listing_cont.view_type_full_2').css('opacity', '1')", 600);

        if (thumb_item.is('.current_item')) {} else {
            thumb_item.first().removeClass('next_item').addClass('current_item');
            content_item.first().addClass('current_item');
            trigger_item.first().addClass('current_item');
        }

        jQuery(trigger_item).on('click', function(){
            var old_current_number = triggers_container.find('.current_item').attr('data-number'),
                current_number = jQuery(this).attr('data-number');

            if (current_number !== old_current_number) {
                jQuery(triggers_container).find('.current_item').removeClass('current_item');
                jQuery(this).addClass('current_item');
                jQuery(content_container).find('.current_item').removeClass('current_item');
                jQuery(content_container).find('[data-number="' + current_number + '"]').addClass('current_item');
                jQuery(image_container).find('.current_item').removeClass('current_item').addClass('prev_item');
                jQuery(image_container).find('[data-number="' + current_number + '"]').removeClass('next_item').addClass('current_item');

                setTimeout("jQuery('.panda_thumbnails_container').find('.prev_item').removeClass('prev_item').addClass('next_item')", 600);
            }
        });

        jQuery('body').on('wheel', function(){
            var current_thumb = image_container.find('.current_item'),
                thumb_number = current_thumb.attr('data-number'),
                counter = container.attr('data-counter');

            if (counter == '1') {
                counter++;
                jQuery(container).attr('data-counter', counter);

                if (thumb_number < items_count) {
                    jQuery(image_container).find('.current_item').removeClass('current_item').addClass('prev_item').next().removeClass('next_item').addClass('current_item');
                    jQuery(content_container).find('.current_item').removeClass('current_item').next().addClass('current_item');
                    jQuery(triggers_container).find('.current_item').removeClass('current_item').next().addClass('current_item');
                } else {
                    jQuery(image_container).find('.current_item').removeClass('current_item').addClass('prev_item');
                    jQuery(thumb_item).first().removeClass('next_item').addClass('current_item');
                    jQuery(content_container).find('.current_item').removeClass('current_item');
                    jQuery(content_item).first().addClass('current_item');
                    jQuery(triggers_container).find('.current_item').removeClass('current_item');
                    jQuery(trigger_item).first().addClass('current_item');
                }

                setTimeout("jQuery('.panda_thumbnails_container').find('.prev_item').removeClass('prev_item').addClass('next_item')", 600);
                setTimeout("jQuery('.panda_portfolio_listing_cont.view_type_full_2').attr('data-counter', '1')", 1000);
            }
        });
    });
}