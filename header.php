<?php $u_assets_path_blog = get_template_directory_uri() . '/assets/blog'; ?>
<?php $u_assets_path_main = get_template_directory_uri() . '/assets/main'; ?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<title><?= get_bloginfo( 'name' ) ?></title>
	<meta name="description" content="<?= get_bloginfo( 'description' ) ?>" />

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">

	<link rel="shortcut icon" href="<?= $u_assets_path_main ?>/img/favicon.ico">

	<link rel="stylesheet"  href="<?= $u_assets_path_main ?>/style/frameworks/slick/slick.css">
	<link rel="stylesheet"  href="<?= $u_assets_path_main ?>/style/frameworks/slick/slick-theme.css">
	<link rel="stylesheet"  href="<?= $u_assets_path_main ?>/style/frameworks/ionicons.min.css">
	<link rel="stylesheet"  href="<?= $u_assets_path_blog ?>/css/libs.min.css">
	<link rel="stylesheet"  href="<?= $u_assets_path_blog ?>/css/main.css">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<?php wp_head(); ?>
</head>
<body class="container-fluid panda_blog_page1">
	<header class="panda_header logo_left panda_sticky_header_on">
		<div class="container header_fw">
			<div class="row">
				<div class="col-md-12 panda_def_header ">
					<div class="panda_logo_cont">
						<a href="<?= get_site_url() ?>" class="panda_image_logo">
							<img width="100" class="logotype" src="<?= $u_assets_path_blog ?>/img/logo.png" alt="">
							<img width="100" class="logotype-retina" src="<?= $u_assets_path_blog ?>/img/logo-retina.png" alt="">
						</a>
					</div>
					<nav class="panda_menu_cont">
						<div class="menu-main-menu-container">
							<ul id="menu-main-menu" class="panda_menu">
								<li class="menu-item"><a href="<?= get_site_url() ?>/#home">главная</a></li>
								<li class="menu-item"><a href="<?= get_site_url() ?>/#about">о компании</a></li>
								<li class="menu-item"><a href="<?= get_site_url() ?>/#skills">навыки</a></li>
								<li class="menu-item"><a href="<?= get_site_url() ?>/#services">услуги</a>
									<ul class="sub-menu">
										<li class="menu-item"><a href="<?= get_site_url() ?>/#services" target="_blank">SEO</a></li>
										<li class="menu-item"><a href="<?= get_site_url() ?>/#services" target="_blank">PR</a></li>
										<li class="menu-item"><a href="<?= get_site_url() ?>/#services" target="_blank">Контекст</a></li>
										<li class="menu-item"><a href="<?= get_site_url() ?>/#services" target="_blank">SMM</a></li>
										<li class="menu-item"><a href="<?= get_site_url() ?>/#services" target="_blank">Наружка</a></li>
										<li class="menu-item"><a href="<?= get_site_url() ?>/#services" target="_blank">Digital</a></li>
										<li class="menu-item"><a href="<?= get_site_url() ?>/#services" target="_blank">Сайты</a></li>
										<li class="menu-item"><a href="<?= get_site_url() ?>/#services" target="_blank">ПО</a></li>
									</ul>
								</li>
								<li class="menu-item"><a href="<?= get_site_url() ?>/#portfolio">портфолио</a></li>
								<li class="menu-item"><a href="<?= get_site_url() ?>/blog">новости</a></li>
								<li class="menu-item"><a href="<?= get_site_url() ?>/#contacts">контакты</a></li>
							</ul>
						</div>
						<div class="clear"></div>
					</nav>
				</div>
				<div class="mobile_header col-md-12">
					<a href="#" class="panda_image_logo"><img width="100" src="<?= $u_assets_path_blog ?>/img/logo.png" alt=""></a>
					<a href="javascript:void(0)" class="btn_mobile_menu">
						<span class="panda_menu_line1"></span>
						<span class="panda_menu_line2"></span>
						<span class="panda_menu_line3"></span>
					</a>
				</div>

			</div>
		</div>
	</header>
	
	<div id="panda_post9" class="container panda_blog_listing<?= (is_single()) ? ' panda_blog_post' : '' ?>">
		<div class="row panda_main">
			<div class="col-md-9 panda_content ">